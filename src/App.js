import React from "react";
import "./App.css";
import HomeAuthentication from "./pages/Home/HomeAuthentication";
import "semantic-ui-css/semantic.min.css";
import "antd/dist/antd.css";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";

function App() {
  return <HomeAuthentication></HomeAuthentication>;
}

export default App;
