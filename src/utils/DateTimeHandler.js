export const generateDateArray = (startDate, range) => {
    let result = [];
    let day = new Date(startDate);
    for(let i = 1; i <= range; i++) {
        result.push(day);
        let nextDay = new Date(day);
        nextDay.setDate(day.getDate() + 1);
        day = nextDay;
    }
    return result;
}