// return the user data from the local storage
export const getUser = () => {
  const userStr = localStorage.getItem("user");
  if (userStr) {
    return JSON.parse(userStr);
  } else {
    return null;
  }
};

// return the token from the local storage
export const getToken = () => {
  return localStorage.getItem("token") || null;
};

// remove the token and user from the local storage
export const removeUserSession = () => {
  localStorage.removeItem("token");
  localStorage.removeItem("user");
};

// set the token and user from the local storage
export const setUserSession = (token, user) => {
  localStorage.setItem("token", token);
  localStorage.setItem("user", JSON.stringify(user));
};

export const clearStorage = () => {
  let token = localStorage.getItem('token');
  let user = localStorage.getItem('user');
  localStorage.clear();
  localStorage.setItem('token', token);
  localStorage.setItem('user',user);
}

const serverURL = 'http://54.169.72.199:5000';
// const serverURL = 'http://localhost:5000';
export default serverURL;
