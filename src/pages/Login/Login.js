import React, { useState } from "react";
import axios from "axios";
import Avatar from "@material-ui/core/Avatar";
import CssBaseline from "@material-ui/core/CssBaseline";
import Link from "@material-ui/core/Link";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import styled from "styled-components";
import './Login.css';
import { setUserSession } from "../../utils/Common";
import serverURL from "../../utils/Common";

function Login(props) {
  const [loading, setLoading] = useState(false);
  const username = useFormInput("");
  const password = useFormInput("");
  const [error, setError] = useState(null);

  // handle button click of login form
  const handleLogin = () => {
    setError(null);
    setLoading(true);
    axios
      .post(`${serverURL}/auth/login`, {
        username: username.value,
        password: password.value,
      })
      .then((response) => {
        setLoading(false);
        setUserSession(response.data.data.token, response.data.data.user);
        window.location.href = "/";
      })
      .catch((error) => {
        setLoading(false);
        if (error.reponse === 401) setError(error.response.data.message);
        else setError(error.response.data.message);
      });
  };

  const useStyles = makeStyles((theme) => ({
    image: {
      backgroundImage:
        "url(http://thuthuatphanmem.vn/uploads/2018/05/23/wallpaper-4k-hinh-nen-4k-mot-goc-ven-bien-dep_100010251.jpg)",
      backgroundRepeat: "no-repeat",
      backgroundColor:
        theme.palette.type === "light"
          ? theme.palette.grey[50]
          : theme.palette.grey[900],
      backgroundSize: "cover",
      backgroundPosition: "center",
    },
    paper: {
      margin: theme.spacing(8, 4),
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: "100%", // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));

  const classes = useStyles();
  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={7} className={classes.image} />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <form className={classes.form}>
            <h3>Login</h3>
            <div className="form-group">
        
              <input
                type="text"
                className="form-control"
                placeholder="Username"
                {...username}
              />
            </div>
            <div className="form-group">
           
              <input
                type="password"
                className="form-control"
                placeholder="Password"
                {...password}
              />
            </div>
            {error && (
              <>
                <small style={{ color: "red", display: "block", textAlign: "start" }}>{error}</small>
              </>
            )}
            <button
              type="submit"
              className="button button1"
              loadingtext="Logging in..."
              onClick={handleLogin}
              disabled={loading}
            >
              Login
            </button>
            <Grid container>
              
              <Grid item>
                <Link href="/register" variant="body2">
                  {"Don't have an account? Register"}
                </Link>
              </Grid>
            </Grid>
          </form>
        </div>
      </Grid>
    </Grid>
  );
}

const useFormInput = (initialValue) => {
  const [value, setValue] = useState(initialValue);
  const handleChange = (e) => {
    setValue(e.target.value);
  };
  return {
    value,
    onChange: handleChange,
  };
};

export default Login;
