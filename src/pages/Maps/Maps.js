import React, { Component } from "react";
import GoogleMapReact from "google-map-react";
import { Dropdown, Grid, Segment } from "semantic-ui-react";
import axios from "axios";
import Marker from "./Marker";
import "./Map.css";
import serverURL from "../../utils/Common"
class Maps extends Component {
	constructor(props) {
		super(props);
		this.state = {
			spots: [],
			listSpot: [],
			ids: String(window.location.href).slice(26, 50),
			value: "5ea69cda3f3b270f908e2cb8",
			lats: "",
			lngs: "",
			name: "",
			address: "",
			id: ""
		};
	}
	componentDidMount() {
		axios
			.get(`${serverURL}/distance/nearby/5000/${this.state.ids}`)
			.then((response) => {
				this.setState({ spots: response.data.data });
			})
			.catch((error) => console.log(error));
		axios
			.get(`${serverURL}/spot/${this.state.ids}`)
			.then((response) => {
				this.setState({ listSpot: response.data.data[0] });

			})
			.catch((error) => console.log(error));
	}
	handleChange = (e, { value }) => this.setState({ value: value });


	render() {
		const categoryOptions = [
			{
				key: "1",
				text: "Stay",
				value: "5ea69cda3f3b270f908e2cb8",
			},
			{
				key: "2",
				text: "Food",
				value: "5ea69cce3f3b270f908e2cb7",
			},
			{
				key: "3",
				text: "Special",
				value: "5ea69c8d3f3b270f908e2cb6",
			},
		];
		const getMapOptions = {
			disablaeDefaultUI: true,
			mapTypeControl: true,
			streetViewControl: true,
			styles: [
				{
					featureType: "poi",
					elementType: "labels",
					stylers: [{ visibility: "on" }],
				},
			],

		};
		const { value, spots } = this.state;
		const spotsNear = spots?.filter(spot => spot?.category?._id === value)?.map(spot => <div className="card-infor">
			<div className="item-images" >
				<img className="images-setup" src={spot.cover_image} onClick={() => this.setState({ name: spot.name }) || this.setState({ address: spot.address }) || this.setState({ id: spot._id })} />
			</div>
			<a className="name-spot-map" href={"/spot/" + `${spot._id}` + "/detail"}>{spot.name}</a>
			<div className="address-spot-map" onClick={() => this.setState({ name: spot.name }) || this.setState({ address: spot.address }) || this.setState({ id: spot._id })}>{spot.address}</div>
		</div>)

		
		return (
			<div className="ui-map">
				<div className="header-map">
					<div className="info-map">
						All Spots nearby {this.state.listSpot.name}
					</div>
				</div>
				<div className="body-ui-map">
					<div className="list-category ">
						<Dropdown
							className="dropdown-setup"
							width="290px"
							onChange={this.handleChange}
							options={categoryOptions}
							placeholder="Choose an option"
							selection
							value={value}
						/>
						{spotsNear}
					</div>
					<div
						style={{
							height: "600px",
							width: "75%",
						}}
					>

						<GoogleMapReact
							bootstrapURLKeys={{
								key: "AIzaSyCXvbLCXHsiGHMRxbC_JLn-2wnKOPdrnsE",
							}}
							defaultCenter={{ lat: 16.052881, lng: 108.217684 }}
							defaultZoom={14}
							options={getMapOptions}
						>


							<Marker
								lat={this.state.listSpot.latitude}
								lng={this.state.listSpot.longtitude}
								name={this.state.listSpot.name}
								address={this.state.listSpot.address}
								color="red"
							/>

							{this.state.spots.filter(spot => spot?.category?._id === value)?.map(spot => (
								<Marker
									lat={spot.latitude}
									lng={spot.longtitude}
									name={spot.name}
									address={spot.address}
									color={this.state.id === spot._id ? "Gold" : "#4caf50"}

								/>
							))
							}
						</GoogleMapReact>
					</div>
				</div>
			</div>
		);
	}
}
export default Maps;
