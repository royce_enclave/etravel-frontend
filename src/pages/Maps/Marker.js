import React from 'react';
import './Marker.css';

const Marker = (props) => {
    const { color, name, address, link} = props;
    return (
        <div>
            <div
                className='pin bounce'
                style={{ backgroundColor: color, cursor: 'pointer' }}
                title={ "Name: " + name + "\n"+
                        "Address: " + address }
                
                 />
            <div className="pulpe" />
        </div>
    )

}
export default Marker;