import React, { Component } from 'react';
import Maps from "./Map";
import "./TripDetail.css";
class MapView extends Component {
    render() {
        const { previousLongtitude, previousLatitude, nextLongtitude, nextLatitude , latSpot, lngSpot } = this.props;
        return (
            <div className="map-trip-view">
                <div className="map-trip-view-header">Map</div>
                <div className="map-setup"> <Maps previousLongtitude={previousLongtitude} previousLatitude={previousLatitude} nextLongtitude={nextLongtitude} nextLatitude={nextLatitude} latSpot={latSpot} lngSpot={lngSpot} /></div>
            </div>
        );
    }
}

export default MapView;