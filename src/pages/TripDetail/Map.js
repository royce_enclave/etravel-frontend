import React, { Component } from 'react';
import { GoogleApiWrapper } from 'google-maps-react';
import "./TripDetail.css";
import _ from "lodash";

class Maps extends Component {
    render() {
        const { previousLongtitude, nextLongtitude, previousLatitude, nextLatitude, latSpot, lngSpot } = this.props;
        // console.log(this.props)
        //console.log(this.state.columnOrder.map((columnId) =>this.state.columns[columnId].spotIds.map(spotId => this.state.spots[spotId]).map(spot =>  spot.latitude).map(lat=> lat)));
        //console.log(this.state.columnOrder.map((columnId) =>this.state.columns[columnId].spotIds.map(spotId => this.state.spots[spotId]).map(spot => spot.longtitude)));
        const google = window.google;
        const { compose, withProps, lifecycle } = require("recompose");
        const {
            withScriptjs,
            withGoogleMap,
            GoogleMap,
            DirectionsRenderer,
            Marker
        } = require("react-google-maps");
        const MapWithADirectionsRenderer = compose(
            withProps({
                googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyCXvbLCXHsiGHMRxbC_JLn-2wnKOPdrnsE&v=3.exp&libraries=geometry,drawing,places",
                loadingElement: <div style={{ height: `100%` }} />,
                containerElement: <div style={{ height: `920px` }} />,
                mapElement: <div style={{ height: `100%` }} />,
            }),
            withScriptjs,
            withGoogleMap,
            lifecycle({
                componentDidMount() {
                    const directionsService = new google.maps.DirectionsService();
                    directionsService.route({
                        destination: new google.maps.LatLng(previousLatitude, previousLongtitude),
                        origin: new google.maps.LatLng(nextLatitude, nextLongtitude),
                        travelMode: google.maps.TravelMode.DRIVING,
                    }, (result, status) => {
                        if (status === google.maps.DirectionsStatus.OK) {
                            this.setState({
                                directions: result,
                            });
                        } else {
                            console.error(`error fetching directions ${result}`);
                        }
                    });
                }
            })
        )(props =>
            <GoogleMap
                zoom={14}
                defaultCenter={new google.maps.LatLng(16.054407, 108.202164)}
            >
                {props.directions && <DirectionsRenderer directions={props.directions} />}
                <Marker position={{ lat: latSpot, lng: lngSpot }} />
            </GoogleMap>
        );
        return (
            <div>
                <MapWithADirectionsRenderer />
            </div>
        );
    }
}

export default GoogleApiWrapper({
    apiKey: ("AIzaSyCXvbLCXHsiGHMRxbC_JLn-2wnKOPdrnsE")
})(Maps)