import React from 'react'
import { Rating } from 'semantic-ui-react'

const RatingExampleDisabled = () => (
  <Rating defaultRating={4} maxRating={5} disabled />
)

export default RatingExampleDisabled
