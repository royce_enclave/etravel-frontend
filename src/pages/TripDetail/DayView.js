import React, { Component } from 'react';
import _ from "lodash";
import { Button } from 'semantic-ui-react'
import "./TripDetail.css";
import { generateDateArray } from '../../utils/DateTimeHandler'
import { default as NumberFormat } from "react-number-format";
class DayView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: 0,
            active: true,
        }
    }
    handleClick = (index) => {
        this.setState({
            selected: index
        })
    }
    change (){
        this.setState({active: !this.state.active})
    }
    render() {
        const { selected } = this.state;
        let btn_class = this.state.active?" day" :" day-toggled";
        const { schedules, dateStart, range } = this.props;
        const daysArray = generateDateArray(dateStart, range);
        const daysResult = daysArray?.map(day => <div className={daysArray.indexOf(day) === selected ? "day-active": "day"} onClick={() => this.handleClick(daysArray.indexOf(day))}>{`${(day.getDate() < 10 ? "0" + day.getDate() : day.getDate())}-${(day.getMonth() < 10 ? "0" + day.getMonth() : day.getMonth())}`}</div>)
        const dayShow = daysArray?.map(day => daysArray.indexOf(day) === selected && <div className="title-header" > Day {selected + 1}| {`${(day.getDate() < 10 ? "0" + day.getDate() : day.getDate())}-${(day.getMonth() < 10 ? "0" + day.getMonth() : day.getMonth()) + "-" + day.getFullYear()}`}</div>)
        const schedule = schedules[selected];
        const spots = schedule?.spots;
        const prices = spots?.map(spot => spot.price);
        const reducer = (accumulator, currentValue) => accumulator + currentValue;
        const totalPrice = prices?.reduce(reducer);
        const totalSpot = spots?.length;
        const formatter = new Intl.NumberFormat('ja-JP', {
            minimumFractionDigits: 0
        })
        return (
            <>
                <div className="day-view-side">
                    {daysResult}

                </div>
                <div className="day-view ">
                    <div className="header-dayview">
                        <div className="infor-header">
                            {dayShow}
                            <div><strong>Spots: </strong> {totalSpot}</div>
                            <div><strong>Total Cost: </strong>~VND {formatter.format(totalPrice)}</div>
                        </div>
                    </div>
                    <div className="item-dayview">
                        {
                            spots?.map(spot => (
                                <>
                                    <div className="item-header-dayview">
                                        <div className="moving-infor">
                                            <div className="start-time">{`${((Math.floor(spot.current_time / 60)) < 10 ? "0" + (Math.floor(spot.current_time / 60)) : (Math.floor(spot.current_time / 60))) + ":"
                                                + ((Math.floor(spot.current_time) - Math.floor(spot.current_time / 60) * 60) < 10 ? "0" + (Math.floor(spot.current_time) - Math.floor(spot.current_time / 60) * 60)
                                                    : (Math.floor(spot.current_time) - Math.floor(spot.current_time / 60) * 60))}`}</div>
                                        </div>
                                        <div className="item-img" ><img className="img-setup" src={spot.cover_image} /></div>
                                        <div className="item-infor" onClick={() => this.props.getDataBack(spots && spots[spots.indexOf(spot)], spot)}><h5>{spot.name}</h5></div>
                                    </div>
                                    <div className="item-bodys">
                                        <div className="moving-distance">
                                            <div>{Math.round(spot.distance * 100) / 100}km</div>
                                            <div>{Math.round(String(spot.moving_time).slice(0, 4))}m</div>
                                        </div>
                                        <div className="item-body">
                                            <div className="address-item"><strong>Address:</strong> {spot.address}</div>
                                            <div className="stayfor"><strong>Time duration:</strong>  {(Math.floor(spot.time_duration)) < 59
                                                ? (Math.floor(spot.time_duration)) + "m"
                                                : `${(Math.floor(spot.time_duration / 60)) +
                                                "h " +
                                                ((Math.floor(spot.time_duration) - Math.floor(spot.time_duration / 60) * 60) > 0 ? (Math.floor(spot.time_duration) - Math.floor(spot.time_duration / 60) * 60) + "m" : "")}`}</div>
                                            <div className="cost"><strong>Cost:</strong> ~VND  {formatter.format(spot.price)}</div>
                                            <div className="member-rating">
                                                {spots[spots.indexOf(spot) - 1] &&
                                                    <Button size="mini" inverted color="blue" content='Back' icon='left arrow' labelPosition=' left ' onClick={() => this.props.getDataBack(spots && spots[spots.indexOf(spot) - 1], spot)} />}
                                                {spots[spots.indexOf(spot) + 1] &&
                                                    <Button size="mini" inverted color="blue" content='Next' icon='right arrow' labelPosition=' right' onClick={() => this.props.getDataBack(spots && spots[spots.indexOf(spot) + 1], spot)} />}
                                            </div>
                                        </div>
                                    </div>
                                </>
                            )
                            )}
                    </div>
                </div>
            </>
        );
    }
}
export default DayView;