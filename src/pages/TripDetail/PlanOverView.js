import React, { Component } from 'react';
import Scrollbar from "react-scrollbars-custom";
import axios from "axios";
import _ from "lodash";
import "./TripDetail.css";
import serverURL from "../../utils/Common"
class PlanOverView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ids: String(window.location.href).slice(27, 51),
            columnOrder: [],
        };
    }
    componentDidMount() {
        axios.get(`${serverURL}/trip/${this.state.ids}`).then((res) => {
            let initialData;
            initialData = res.data.data;

            let result = [];
            for (let item of initialData.schedules) {
                for (let test of item.spots) {
                    result.push(test);
                }
            }
            let spot = {
                spots: _.mapKeys(Object.assign({}, result), (key, value) => {
                    return key._id
                })
            }
            const column = {
                columns: Object.assign(
                    {},
                    initialData.schedules.reduce((acc, cur, index) => {
                        return [
                            ...acc,
                            {
                                id: index.toString(),
                                spotIds: cur.spots.map((item) => item._id),
                            },
                        ];
                    }, [])
                ),
            };
            const columnOrder = { columnOrder: Object.keys(initialData.schedules) };
            const newInitialData = {
                ...spot,
                ...column,
                ...columnOrder,

            };
            this.setState(newInitialData);
        })
    }
    render() {
        return (
                <div className="plan-overview">
                    <div className="day-overview-active" sstyle="width: auto;">
                        {
                            this.state.columnOrder.map((columnId) => (
                                <div className="day-overview">
                                    <div className="day-overview-header"> Day {Math.floor(columnId) + 1}</div>
                                    {
                                        this.state.columns[columnId].spotIds.map(spotId => this.state.spots[spotId]).map(spot => (
                                            <div className="item-day">
                                                <div className="time">{`${((Math.floor(spot.current_time / 60)) < 10 ? "0" + (Math.floor(spot.current_time / 60)) : (Math.floor(spot.current_time / 60))) + ":"
                                                    + ((Math.floor(spot.current_time) - Math.floor(spot.current_time / 60) * 60) < 10 ? "0" + (Math.floor(spot.current_time) - Math.floor(spot.current_time / 60) * 60)
                                                        : (Math.floor(spot.current_time) - Math.floor(spot.current_time / 60) * 60))}`}</div>
                                                <div className="dot"></div>
                                                <div className="name-location">{spot.name} </div>
                                            </div>
                                        ))
                                    }
                                </div>
                            ))
                        }
                        <div />
                    </div>

                </div>
        );
    }
}

export default PlanOverView;