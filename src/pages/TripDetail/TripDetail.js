import React, { Component } from "react";
import PlanViewHeader from "./PlanViewHeader";
import PlanOverView from "./PlanOverView";
import MapView from "./MapView";
import DayView from './DayView';
import axios from "axios";
import "./TripDetail.css";
import serverURL from "../../utils/Common"
class TripDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ids: String(window.location.href).slice(27, 51),
      data: "",
      previousSpot: "",
      nextSpot: "",
      locationSpot: ""
    };
  }

  componentDidMount() {
    axios.get(`${serverURL}/trip/${this.state.ids}`).then((res) => {
      this.setState({
        data: res.data.data
      })
    }).catch(error => console.log(error))
  }

  handleGetData = (previousSpot, nextSpot, locationSpot) => {
    this.setState({
      previousSpot, nextSpot, locationSpot
    });
  }

  render() {
    const { data, previousSpot, nextSpot, locationSpot } = this.state;
    const { schedules, date_start, number_of_day } = data;
    return (
      <div>
        <div className="plan-view-header-wrapper">
          <PlanViewHeader />
          <div className="ui-itineraries">
            <PlanOverView />
            <div className="day-view-wrapper">
              <div className="plan-view-header-ui">Schedule</div>
              <div className="days-view">
                <DayView schedules={schedules ? schedules : []} dateStart={date_start} range={number_of_day} getDataBack={this.handleGetData} />
              </div>
            </div>
            <MapView latSpot={locationSpot ? locationSpot.latitude : 0} lngSpot={locationSpot ? locationSpot.longtitude : 0} previousLongtitude={previousSpot ? previousSpot.longtitude : 0} previousLatitude={previousSpot ? previousSpot.latitude : 0} nextLongtitude={nextSpot ? nextSpot.longtitude : 0} nextLatitude={nextSpot ? nextSpot.latitude : 0} />
          </div>
        </div>
      </div>
    );
  }
}

export default TripDetail;
