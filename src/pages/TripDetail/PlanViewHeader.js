import React, { Component } from "react";
import { Icon, Button } from "semantic-ui-react";
import axios from "axios";
import "./TripDetail.css";
import { BrowserRouter as Router, Link } from "react-router-dom";
import { default as NumberFormat } from "react-number-format";
import { getToken } from "../../utils/Common"
import serverURL from "../../utils/Common"
import { Tag } from "antd";
class PlanViewHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ids: String(window.location.href).slice(27, 51),
      listspots: [],
      namePeople: ""
    };
  }

  componentDidMount() {
    axios
      .get(`${serverURL}/trip/${this.state.ids}`)
      .then((response) => {
        this.setState({
          listspots: response.data.data,
        });
      })
      .catch((error) => console.log(error));
  }

  render() {
    const peoples = this.state.listspots.adult + this.state.listspots.kid;
    const formatter = new Intl.NumberFormat('ja-JP', {
      minimumFractionDigits: 0
    })
    const budget = formatter.format(this.state.listspots.budget)
    // let categories = this.state.listspots.sub_categories;

    // categories= categories?categories.filter(item => {
    //   return !(item.name =="breakfast"|| item.name =="lunch"|| item.name =="dinner"|| item.name =="morning"|| item.name =="afternoon"|| item.name =="night");
    // }):[];


    return (
      <div className="plan-view-header">
        <div className="large-header">
          <div className="content">{this.state.listspots.name}</div>
          <div className="sub-header">
            <div className="text-sub-header"><strong>Number of People:</strong> {peoples} people </div>
            <div className="text-money"><strong>Budget:</strong> VND {budget}| <strong>Created by: </strong> {this.state.listspots.creator?.full_name}</div>
            <div className="text-money"></div>
          </div>
          <div className="bt-setup">
            <Button color="blue" disabled={!getToken()} icon labelPosition="left" href={"/trip/" + `${this.state.ids}` + "/edit"}>
              <Icon name="edit" />
              Clone
            </Button>
          </div>
        </div>
      </div >
    );
  }
}

export default PlanViewHeader;
