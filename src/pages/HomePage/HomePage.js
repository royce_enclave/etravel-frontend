import React from "react";
import Banner from "../../components/Banner/Banner";
import Carousel from "../../components/Carousel/Carousel";
import CarouselTrip from "../../components/Carousel/CarouselTrip";

function HomePage() {
  return (
    <div>
      <div>
        <Banner></Banner>
      </div>
      <div>
        <br/>
        <h1
          style={{
            padding: "0 10%",
            fontWeight: "600",
            lineHeight: "2.5",
            textAlign: " start",
          }}
        >
          Popular Spots
        </h1>
        <Carousel></Carousel>
      </div>
      <div>
        <br />
        <h1
          style={{
            padding: "0 10%",
            fontWeight: "600",
            lineHeight: "2.5",
            textAlign: " start",
          }}
        >
          Hot Trips
        </h1>
        <CarouselTrip></CarouselTrip>
       
      </div>

      <section
        className="showcase"
        style={{ marginLeft: "10%", marginRight: "10%", marginTop: "5%",  boxShadow:"rgba(0, 0, 0, 0.3) 0px 0px 6px 0px", padding:"2%", backgroundColor:"rgb(255, 255, 255)" }}
      >
        <div className="container-fluid p-0">
          <div className="row no-gutters">
            <div
              className="col-lg-6 order-lg-2 text-white showcase-img"
              style={{
                backgroundImage:
                  "url('https://anywhereandaverywhere.com/wp-content/uploads/2019/02/IMG_8647.jpeg')",
                backgroundSize: "cover",
                height: "400px",
             
              }}
            ></div>
            <div className="col-lg-6 order-lg-1 my-auto showcase-text">
              <p
                className="lead mb-0"
                style={{
                  textAlign: "justify",
                  paddingRight: "20px",
                  fontStyle: "Italic",
                }}
              >
                <h2>PLAN YOUR TRIP IN DA NANG</h2>
                <br />
                Planning a trip to Da Nang, Vietnam? Da Nang has a lot to offer
                all types of visitors! If you’re visiting Da Nang on holiday or
                plan on settling down for awhile, check out this list of things
                to do in Da Nang including Da Nang food recommendations, Da Nang
                nightlife and day time adventure trips.
              </p>
            </div>
          </div>
          <div className="row no-gutters">
            <div
              className="col-lg-6 text-white showcase-img"
              style={{
                backgroundImage:
                  "url('http://thuthuatphanmem.vn/uploads/2018/05/23/wallpaper-4k-hinh-nen-4k-phao-hoa-ruc-ro-sac-mau-cuc-dep_100017333.jpg')",
                backgroundSize: "cover",
                height: "400px",
               
              }}
            ></div>
            <div
              className="col-lg-6 my-auto showcase-text"
              style={{ paddingLeft: "20px", fontStyle: "Italic",textAlign: "justify" }}
            >
              <h2>ENJOY YOUR VACATION</h2>
              <p className="lead mb-0">
                <br />
                Live the lush life on top of one of Da Nang’s many rooftop
                pools. A city full of hotels (with more being built every single
                day) means there’s a whole lot of rooftop pools to be visited.
              </p>
            </div>
          </div>
          <div className="row no-gutters">
            <div
              className="col-lg-6 order-lg-2 text-white showcase-img"
              style={{
                backgroundImage:
                  "url('https://daynauan.info.vn/wp-content/uploads/2019/05/mi-quang-ga.jpg')",
                backgroundSize: "cover",
                height: "400px",
               
              }}
            ></div>
            <div
              className="col-lg-6 order-lg-1 my-auto showcase-text"
              style={{
                textAlign: "justify",
                paddingRight: "20px",
                fontStyle: "Italic",
              }}
            >
              <h2>ENJOY FOOD </h2> <br />
              <p className="lead mb-0">
                Da Nang food is no joke — especially the Quang Noodle! The bowl
                translates to “Noodle” and after giving it a try, you’re going
                to wish you ate it sooner. If you’re needing more ideas of what
                to do in Da Nang – make sure eating Quang Noodle is on the list.
              </p>
            </div>
          </div>
        </div>
      </section>

      <section
        className="testimonials text-center"
        style={{ marginLeft: "10%", marginRight: "10%", marginTop: "5%",  boxShadow:"rgba(0, 0, 0, 0.3) 0px 0px 6px 0px", padding:"2%",backgroundColor:"rgb(255, 255, 255)" }}
      >
        <div className="container" style={{paddingTop:"0"}}>
          <h2 className="mb-5">People say</h2>
          <div className="row">
            <div className="col-lg-4">
              <div className="testimonial-item mx-auto mb-5 mb-lg-0">
                <img
                  className="img-fluid rounded-circle mb-3"
                  style={{ width: "7rem", height: "7rem" }}
                  src="https://e-travel-s3-bucket.s3.ap-southeast-1.amazonaws.com/image/7ab7245a2932e760f7cf66631163906d"
                  alt=""
                />
                <h5>Royce</h5>
                <p className="font-weight-light mb-0">"This is fantastic!</p>
                <p className="font-weight-light mb-0">
                  This website gave me my trips fastly"
                </p>
              </div>
            </div>
            <div className="col-lg-4">
              <div className="testimonial-item mx-auto mb-5 mb-lg-0">
                <img
                  className="img-fluid rounded-circle mb-3"
                  style={{ width: "7rem", height: "7rem" }}
                  src="https://e-travel-s3-bucket.s3.ap-southeast-1.amazonaws.com/image/c93e384bfcd3c46c4890177603123cd0"
                  alt=""
                />
                <h5>Will</h5>
                <p className="font-weight-light mb-0">"Great with me!</p>
                <p className="font-weight-light mb-0">
                  UI of this website is easy to use and clearly"
                </p>
              </div>
            </div>
            <div className="col-lg-4">
              <div className="testimonial-item mx-auto mb-5 mb-lg-0">
                <img
                  className="img-fluid rounded-circle mb-3"
                  style={{ width: "7rem", height: "7rem" }}
                  src="https://e-travel-s3-bucket.s3.ap-southeast-1.amazonaws.com/image/7694fd17d9b9596e9c62357dc3b84938"
                  alt=""
                />
                <h5>Quang</h5>
                <p className="font-weight-light mb-0">
                  "So convenient for planning trips!
                </p>
                <p className="font-weight-light mb-0">
                  And I will recommend for my friends and family!"
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <br />
    </div>
  );
}

export default HomePage;
