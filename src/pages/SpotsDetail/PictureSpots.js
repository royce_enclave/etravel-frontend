import React, { Component } from "react";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import axios from "axios";
import Slider from "react-slick";
import "./SpotsDetail.css";
import serverURL from "../../utils/Common"

class PictureSpots extends Component {
  constructor(props) {
    super(props);
    this.state = {
      spots: [],
      ids: String(window.location.href).slice(27, 51),
    };
  }

  componentDidMount() {
    axios
      .get(`${serverURL}/spot/${this.state.ids}`)
      .then((response) => {
        this.setState({ spots: response.data.data[0] });
      })
      .catch((error) => console.log(error));
  }
  render() {
    return (
      <div>
        <div>
          <img className="picture-setup" src={this.state.spots.cover_image} />
        </div>
      </div>
    );
  }
}

export default PictureSpots;
