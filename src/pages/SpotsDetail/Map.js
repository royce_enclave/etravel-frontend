import React, { Component } from "react";
import { Map, InfoWindow, Marker, GoogleApiWrapper } from "google-maps-react";
import axios from "axios";
import serverURL from "../../utils/Common"

export class Maps extends Component {
  constructor(props) {
    super(props);
    this.state = {
      spots: [],
      ids: String(window.location.href).slice(27, 51),
    };
  }
  componentDidMount() {
    axios
      .get(`${serverURL}/spot/${this.state.ids}`)
      .then((response) => {
        this.setState({ spots: response.data.data[0] });
      })
      .catch((error) => console.log(error));
  }
  render() {
    const style = {
      width: "100%",
      height: "100%",
    };
    const lats = parseFloat(this.state.spots.latitude);
    const lngs = parseFloat(this.state.spots.longtitude);

    return (
      <Map
        google={this.props.google}
        style={style}
        center={{
          lat: this.state.spots.latitude,
          lng: this.state.spots.longtitude,
        }}
        zoom={13}
        onClick={this.onMapClicked}
      >
        <Marker
          position={{
            lat: this.state.spots.latitude,
            lng: this.state.spots.longtitude,
          }}
        />
      </Map>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: "AIzaSyCXvbLCXHsiGHMRxbC_JLn-2wnKOPdrnsE",
})(Maps);
