import React, { Component, useState } from "react";
import {
  BrowserRouter as Router,
  Switch,
  useRouteMatch,
  Route,
} from "react-router-dom";
import Header from "../../components/Header/Header";
import BigTitle from "./BigTitle";
import PictureSpots from "./PictureSpots";
import Information from "./Information";
import Map from "./Map"; 
import Rating from "./Rating";
import NearPlace from "./NearPlace";
import ImageSpots from "./ImageSpots";
import "./SpotsDetail.css";
import axios from 'axios'
import serverURL from "../../utils/Common"
class SpotsDetail extends Component {
  constructor(){
    super()
    this.state={
      ids: String(window.location.href).slice(27,51),
    }
  }
  // ids= this.href.slice(27,51)
  componentDidMount() {
    axios
      .get(
        `${serverURL}/spot/${this.state.ids}`
      )
      .then(response => {
        this.setState({ spots: response.data.data[0] });
      })
      .catch(error => console.log(error));
  }
  
  render(){
    return (
    
    <div>
        <div className="ui-tineraries">
          {/* <PictureSpots /> */}
          <BigTitle  />
          <Information />
          <div className="map-spots">
            <Map />
          </div>
          <ImageSpots />     
          <Rating />
          <NearPlace />
        </div>
    </div>
  );
}
}

export default SpotsDetail;
