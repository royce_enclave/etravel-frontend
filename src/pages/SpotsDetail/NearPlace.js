import React, { Component } from "react";
import axios from "axios";
import { Rating, Icon } from "semantic-ui-react";
import { BrowserRouter as Router, Link } from "react-router-dom";
import serverURL from "../../utils/Common"

class NearPlace extends Component {
  constructor(props) {
    super(props);
    this.state = {
      spots: [],
      ids: String(window.location.href).slice(27, 51),
      listspots: [],
    };
  }

  componentDidMount() {
    axios
      .get(`${serverURL}/distance/nearby/5000/${this.state.ids}`)
      .then((response) => {
        this.setState({
          spots: response.data.data[0],
          listspots: response.data.data,
        });
      })
      .catch((error) => console.log(error));
  }
  render() {
    return (
      <div className="ui-others-near-place">
        <div className="header-near-place"> Nearby</div>
        <div className="body-near-place">
          <div className="location-near">
            <div className="header-location-near">
              <h3>Hotels</h3>
              <a className="all-location-near" href={"/map/" + `${this.state.ids}` + "/map"} >
                All
                
              </a>

            </div>
            <div className="body-location-near">
              {this.state.listspots
                .filter((item) => item.category.name === "stay")
                .slice(0, 4)
                .map((spot) => (
                  <div className="card-item">
                    <a
                      className="a-setup"
                      href={"/spot/" + spot._id + "/detail"}
                    >
                      <img
                        className="img-card-item"
                        src={spot["cover_image"]}
                      />
                    </a>
                    <div className="info-card-item">
                      <div className="name-card-item">
                        <span>{spot.name}</span>
                        <br />
                        <Rating
                          icon="star"
                          defaultRating={4}
                          maxRating={5}
                          disabled
                        />
                        <div className="address-card-item"></div>
                      </div>
                    </div>
                  </div>
                ))}
            </div>
          </div>
          <div className="location-near">
            <div className="header-location-near">
              <h3>Restaurants</h3>
            </div>
            <div className="body-location-near">
              {this.state.listspots
                .filter((item) => item.category.name === "food")
                .slice(0, 4)
                .map((spot) => (
                  <div className="card-item">
                    <a
                      className="a-setup"
                      href={"/spot/" + spot._id + "/detail"}
                    >
                      <img
                        className="img-card-item"
                        src={spot["cover_image"]}
                      />
                    </a>
                    <div className="info-card-item">
                      <div className="name-card-item">
                        <span>{spot.name}</span>
                        <br />
                        <Rating
                          icon="star"
                          defaultRating={4}
                          maxRating={5}
                          disabled
                        />
                        <div className="address-card-item"></div>
                      </div>
                    </div>
                  </div>
                ))}
            </div>
          </div>
          <div className="location-near">
            <div className="header-location-near">
              <h3>Entertainment venues </h3>
            </div>
            <div className="body-location-near">
              {this.state.listspots
                .filter((item) => item.category.name === "special")
                .slice(0, 4)
                .map((spot) => (
                  <div className="card-item">
                    <a
                      className="a-setup"
                      href={"/spot/" + spot._id + "/detail"}
                    >
                      <img
                        className="img-card-item"
                        src={spot["cover_image"]}
                      />
                    </a>
                    <div className="info-card-item">
                      <div className="name-card-item">
                        <span>{spot.name}</span>
                        <br />
                        <Rating
                          icon="star"
                          defaultRating={4}
                          maxRating={5}
                          disabled
                        />
                        <div className="address-card-item"></div>
                      </div>
                    </div>
                  </div>
                ))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default NearPlace;
