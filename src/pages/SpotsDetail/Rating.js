import React, { Component } from "react";
import { Icon, Progress } from "semantic-ui-react";
import Comments from "./Comments";
import "semantic-ui-css/semantic.min.css";
class Rating extends Component {
  render() {
    return (
      <div className="section-comment">
        <div className="header-section-comment">
          <h2>Comment and rating</h2>
        </div>
        <div className="body-section-comment">
          <div className="rating-overview">
            <div className="number-rating">
              <div className="number">4.5/5</div>
              <div className="number-people">
                5 <Icon disabled name="user" color="blue" />
              </div>
            </div>
            <div className="percent-rating">
              <div className="row-rating">
                <div className="felling-rating">Perfect</div>
                <Progress
                  className="row-percent"
                  percent={90}
                  color="blue"
                  active
                />
              </div>
              <br />
              <div className="row-rating">
                <div className="felling-rating">Good</div>
                <Progress
                  className="row-percent"
                  percent={80}
                  color="blue"
                  active
                />
              </div>
              <br />
              <div className="row-rating">
                <div className="felling-rating">Alright</div>
                <Progress
                  className="row-percent"
                  percent={70}
                  color="blue"
                  active
                />
              </div>
              <br />
              <div className="row-rating">
                <div className="felling-rating">Bad</div>
                <Progress
                  className="row-percent"
                  percent={20}
                  color="blue"
                  active
                />
              </div>
              <br />
              <div className="row-rating">
                <div className="felling-rating">Terrible</div>
                <Progress
                  className="row-percent"
                  percent={10}
                  color="blue"
                  active
                />
              </div>
            </div>
          </div>
          <div className="comment-show">
            <Comments />
          </div>
        </div>
      </div>
    );
  }
}

export default Rating;
