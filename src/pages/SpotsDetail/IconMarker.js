import React from "react";
import { Icon } from "semantic-ui-react";

const IconExampleDisabled = () => (
  <Icon color="red" disabled name="map marker alternate" />
);

export default IconExampleDisabled;
