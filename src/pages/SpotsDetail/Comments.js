import React, { Component } from "react";
import { Icon, Rating, Form, TextArea, Button } from "semantic-ui-react";
import "./SpotsDetail.css";

class Comments extends Component {
  render() {
    return (
      <div>
        <div className="comment">
          <img
            className="avatar-user-comment"
            src="https://e-travel-s3-bucket.s3.ap-southeast-1.amazonaws.com/image/c93e384bfcd3c46c4890177603123cd0"
            alt
          />
          <div className="content-comment">
            <div className="name-user-comment">
              {" "}
              <span>Royce</span>
            </div>
            <div className="time-comment">Yesterday 12:39pm</div>
            <br />
            <Rating
              className="user-rating"
              icon="star"
              defaultRating={4}
              maxRating={5}
              disabled
            />
            <div className="text-comment">It looks so beautiful!</div>
          </div>
        </div>
        <div className="comment">
          <img
            className="avatar-user-comment"
            src="https://e-travel-s3-bucket.s3.ap-southeast-1.amazonaws.com/image/7694fd17d9b9596e9c62357dc3b84938"
             alt
          />
          <div className="content-comment">
            <div className="name-user-comment">
              {" "}
              <span>Will</span>
            </div>
            <div className="time-comment">Last week 11:29am</div>
            <br />
            <Rating
              className="user-rating"
              icon="star"
              defaultRating={4}
              maxRating={5}
              disabled
            />
            <div className="text-comment">I love it!</div>
          </div>
        </div>
        <div className="comment">
          <img
            className="avatar-user-comment"
            src="https://e-travel-s3-bucket.s3.ap-southeast-1.amazonaws.com/image/7ab7245a2932e760f7cf66631163906d"
            alt
          />
          <div className="content-comment">
            <div className="name-user-comment">
              {" "}
              <span>Miller</span>
            </div>
            <div className="time-comment">Today 10:19am</div>
            <br />
            <Rating
              className="user-rating"
              icon="star"
              defaultRating={1}
              maxRating={5}
              disabled
            />
            <div className="text-comment">It looks so bad!</div>
          </div>
        </div>
        <div className="your-comment">
          <img
            className="avatar-user-comment"
            src="https://e-travel-s3-bucket.s3.ap-southeast-1.amazonaws.com/image/e44b033a78eae9ffce9c37df223baf47"
            alt
          />
          <div className="content-your-comment">
            <div className="rate-this-place"> Rate this place</div>
            <Rating
              className="rating-this-place"
              icon="star"
              defaultRating={3}
              maxRating={5}
            />
            <Form className="comment-this-place">
              <TextArea
                className="height-comment-this-place"
                placeholder="Hi Basil, please add your comment!"
              />
            </Form>
            <div className="btn-update">
              <Button className="btn-update" primary>Comment</Button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Comments;
