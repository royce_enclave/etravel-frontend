import React, { Component } from "react";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import axios from "axios";
import Slider from "react-slick";
import "./SpotsDetail.css";
import { IoIosArrowForward, IoIosArrowBack } from "react-icons/io";
import serverURL from "../../utils/Common"

class PictureSpots extends Component {
  constructor(props) {
    super(props);
    this.state = {
      spots: [],
      ids: String(window.location.href).slice(27, 51),
    };
  }

  componentDidMount() {
    axios
      .get(`${serverURL}/spot/${this.state.ids}`)
      .then((response) => {
        this.setState({
          spots: response.data.data[0],
          listspots: response.data.data,
        });
      })
      .catch((error) => console.log(error));
  }

  render() {
    const settings = {
      infinite: true,
      autoplay: true,
      // autoplaySpeed: 1900,
      className: "center",
      centerPadding: "60px",
      slidesToShow: 3,
      pauseOnHover: true,
      nextArrow: <IoIosArrowForward size={64} color="black" />,
      prevArrow: <IoIosArrowBack size={64} color="black" />,
      speed: 500,
    };
    const images = this.state.spots.images? this.state.spots.images : [];
    const cover_image = this.state.spots.cover_image;
    const photos = [
      {
        name: "picture1",
        url: [images [0]? images[0]: []],
      },
      {
        name: "picture2",
        url:[images [1]? images[1]: []],
      },
      {
        name: "picture3",
        url: [images [2]? images[2]: []],
      },
      {
        name: "picture4",
        url: [cover_image ? cover_image: []],
      },
      
    ];

    return (
      <div className="section-picture-setup">
        <div className="header-picture-setup">Pictures</div>
        <div className="body-picture-setup">
          <Slider {...settings}>
            {photos.map((photo) => {
              return (
                <div className="image-setup" >
                  <img className="image-setup" src={photo.url} />
                </div>              
              );
            })}
          </Slider>
          </div>
      </div >
    );
  }
}

export default PictureSpots;
