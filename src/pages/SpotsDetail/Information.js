import React, { Component } from "react";
import axios from "axios";
import "./SpotsDetail.css";
import serverURL from "../../utils/Common"
class Information extends Component {
  constructor(props) {
    super(props);
    this.state = {
      spots: [],
      ids: String(window.location.href).slice(27, 51),
    };
  }

  componentDidMount() {
    axios
      .get(`${serverURL}/spot/${this.state.ids}`)
      .then((response) => {
        this.setState({ spots: response.data.data[0] });
      })
      .catch((error) => console.log(error));
  }
  render() {
    return (
      <div className="info-spots">
        <div className="info">
          <div className="header-info-trip">
            Introduction
          </div>
          <div className="conten">
            <p className="conten">{this.state.spots.description}</p>
          </div>
        </div>
      </div>
    );
  }
}

export default Information;
