import React, { Component } from "react";
import axios from "axios";
import "./SpotsDetail.css";
import serverURL from "../../utils/Common"
import { Tag } from "antd";
class BigTitle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      spots: [],
      ids: String(window.location.href).slice(27, 51)
    };
  }

  componentDidMount() {
    axios
      .get(`${serverURL}/spot/${this.state.ids}`)
      .then((response) => {
        this.setState({ spots: response.data.data[0] });
        const time = this.state.spots.time_duration / 60;
      })
      .catch((error) => console.log(error));
  }

  render() {
    const formatter = new Intl.NumberFormat('ja-JP', {
      minimumFractionDigits: 0
    })
    const time = this.state.spots.time_duration / 60;
    const money = formatter.format(this.state.spots.price);
    const category = this.state.spots.category ? this.state.spots.category : "";

    return (
      <div className="header-ui-spots">
        <div className="name-spots">{this.state.spots.name}</div>
        <div className="address-spots">
          <div className="address-text"> <strong>Address:</strong> {this.state.spots.address}</div>
          <div className="time-duration-text">
            <strong>Time duration:</strong> {(time) < 1
              ? (this.state.spots.time_duration) + " minutes"
              : (time) + " hours" && (time) === 1 ? (time) + " hour" : (time) + " hours"}
            <div><strong>Cost: </strong>~VND {money}</div>
          </div>

          <div className="d-flex flex-wrap" style={{ marginLeft: "15px", textTransform: "capitalize" }}>
            {
              category.sub_categories ? category.sub_categories.map((sub_category) => (
                <div className="mb-1">
                  <Tag color="blue">{sub_category.name} </Tag>
                </div>
              )) : []
            }
          </div>
        </div>
      </div>
    );
  }
}

export default BigTitle;
