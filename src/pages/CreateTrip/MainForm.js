import React, { Component } from "react";
import BasicInfo from "./BasicInform";
import FoodForm from "./FoodForm";
import StayForm from "./StayForm";
import HobbiesForm from "./HobbiesForm";
import Confirmation from "./ComfirmForm";
import EditTrip from "../EditTrip/EditTrip";

class MainForm extends Component {
  state = {
    step: 1,
    budget: 0,
    day_start: "",
    day_end: "",
    child: 0,
    adult: 0,
  };

  nextStep = () => {
    const { step } = this.state;
    this.setState({
      step: step + 1,
    });
  };

  prevStep = () => {
    const { step } = this.state;
    this.setState({
      step: step - 1,
    });
  };
  handleChange = (input) => (event) => {
    this.setState({ [input]: event.target.value });
  };
  render() {
    const { step } = this.state;
    const { budget, day_start, day_end, child, adult } = this.state;
    const values = { budget, day_start, day_end, child, adult };
    switch (step) {
      case 1:
        return (
          <BasicInfo
            nextStep={this.nextStep}
            handleChange={this.handleChange}
            values={values}
          />
        );
      case 2:
        return (
          <StayForm
          nextStep={this.nextStep}
          prevStep={this.prevStep}
          values={values}
        />
          
        );
      case 3:
        return (
          <FoodForm
          nextStep={this.nextStep}
          prevStep={this.prevStep}
          handleChange={this.handleChange}
          values={values}
        />
        );
      case 4:
        return (
          <HobbiesForm
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            values={values}
            handleChange={this.handleChange}
          />
        );
      case 5:
        return (
          <Confirmation
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            values={values}
            handleChange={this.handleChange}
          />
        );
      case 6:
        return <EditTrip></EditTrip>;
    }
  }
}

export default MainForm;
