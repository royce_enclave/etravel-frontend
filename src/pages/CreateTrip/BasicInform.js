import React, { Component } from 'react';
import styled from 'styled-components';
import Slide from './RangSile';
import DateTime from './DateTime';
import moment from 'moment';

const Container = styled.div`
  padding: 3rem 15%;
`;
const Content = styled.div`
  background: white;
  margin: 1rem 20%;
`;
const Caption = styled.h2`
  text-transform: capitalize;
`;
const Form = styled.form`
  padding: 1rem;
  box-shadow: rgba(0, 0, 0, 0.3) 0px 0px 6px 0px;
`;
const Button = styled.button`
  padding: 1rem 2rem;
  border: 1px solid black;
  background: white;
  outline: none;
  &:hover {
    background: #81ecec;
  }
`;
class BasicInfo extends Component {
  constructor() {
    super();
    this.state = {isValidation: false};
    this.name = this.name.bind(this);
    this.adult = this.adult.bind(this);
    this.kid = this.kid.bind(this);
    this.number_of_day = this.number_of_day.bind(this);
  }
  isValidation(){
    if(this.state.adult <1 || this.state.adult >5){
      alert('Number of adult from 1 to 5')
      return false;
    } 
    else if(this.state.kid <0 || this.state.kid >5){
      alert('Number of kid from 0 to 5');
      return false;
    }
    else if((this.state.budget/this.state.number_of_day)/(this.state.adult+this.state.kid)<=1000000){
      alert("Your budget is low!")
      return false;
    }
    return true
 
  }
  onKeyDown(e){

  }
  name(event) {
    this.setState({ name: event.target.value });
  }
  adult(event) {
    let adult = parseInt(event.target.value);
    this.setState({ adult: adult });
  }
  kid(event) {
    let kid = parseInt(event.target.value);
    this.setState({ kid: kid });
  }
  number_of_day(event) {
    this.setState({ number_of_day: event.target.value });
  }

  saveAndContinue = (e) => {
    if(this.isValidation()){
      // e.preventDefault();
      this.props.nextStep();
      localStorage.setItem('basicform', JSON.stringify(this.state));
    }
  };
  budget = (budget) => {
    this.setState({
      ...this.state,
      budget,
    });
  };
  day_start = (day_start) => {
    this.setState({
      ...this.state,
      day_start: day_start.from,
    });
  };
  day_end = (day_end) => {
    this.setState({
      ...this.state,
      day_end: day_end.to,
    });
  };

  componentDidMount() {
    let prevData = JSON.parse(localStorage.getItem('basicform'));
    console.log('prevData: ', prevData);
    if (prevData === null) {
      this.setState({
        name: '',
        number_of_day: 0,
        budget: 1000000,
        day_start: new Date(),
        day_end: new Date(),
        adult: 0,
        kid: 0,
      });
    } else {
      console.log('test state: ', prevData);
      let adult = parseInt(prevData.adult);
      let kid = parseInt(prevData.kid);
      this.setState({...prevData,
        name: prevData.name,
        number_of_day: prevData.number_of_day,
        budget: prevData.budget,
        day_start: prevData.day_start,
        day_end: prevData.day_end,
        adult: adult,
        kid: kid,
      });
    }
  }
  render() {
    let a = moment(this.state.day_end, 'YYYY-MM-DD');
    let b = moment(this.state.day_start, 'YYYY-MM-DD');
    this.state.number_of_day = moment(a).diff(moment(b), 'day') + 1;
    let data = JSON.parse(localStorage.getItem('basicform')) || [];
    return (
      <Container>
        <Caption>Basic Infomation</Caption>
        <Content>
          <Form>
            <h4 style={{ textAlign: 'start' }}>Budget <strong style={{ color: 'red' }}>*</strong></h4>
            <p style={{fontStyle:"Italic", textAlign:"start"}}>Not including the fees of transporting and staying </p>
            <Slide value={this.budget} />
            <h4 style={{ textAlign: 'start' }}>Duration <strong style={{ color: 'red' }}>*</strong></h4>
            <DateTime day_start={this.day_start} day_end={this.day_end} />
            <h4 style={{ textAlign: 'start' }}> Number of people <strong style={{ color: 'red' }}>*</strong></h4>
            <div style={{ display: 'flex', marginBottom: '5%' }}>
              <div style={{ marginLeft: '5%', width: '36%' }}>
                <h5>Adult</h5>
                <input
                  max={5}
                  min={0}
                  type="number"
                  className="form-control"
                  placeholder="Number of adult"
                  onChange={this.adult}
                  defaultValue={data.adult}
                />
              </div>
              <div style={{ marginLeft: '18%', width: '36%' }}>
                <h5>Kid</h5>
                <input
                  max={5}
                  min={0}
                  type="number"
                  className="form-control"
                  placeholder="Number of kid"
                  onChange={this.kid}
                  defaultValue={data.kid}
                 
                />
              </div>
            </div>
            <Button type="button" onClick={this.saveAndContinue}>Continue</Button>
          </Form>
        </Content>
      </Container>
    );
  }
}

export default BasicInfo;
