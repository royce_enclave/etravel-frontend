import React, { Component } from "react";
import Slider from "react-rangeslider";
import styled from "styled-components";
import "./RangeSlide.css";
import { default as NumberFormat } from "react-number-format";
const Container = styled.div`
  display: flex;
  align-items: center;
  margin-left:3%;
  margin-right:3%;
`;

const Range = styled(Slider)`
  width: 100%;
  margin-left:3%;
`;

class Slide extends Component {
  constructor(props, context) {
    super(props, context);
    let prevData = JSON.parse(localStorage.getItem("basicform"))||[];
  
    this.state = {
      horizontal:prevData.budget,
    };
  }
  handleChangeHorizontal = (range) => {
    this.setState({
      horizontal: range,
    });
    this.props.value(range);
  };
  render() {
    const { horizontal } = this.state;
    const format = (value) => <NumberFormat value={value} displayType={'text'} thousandSeparator={true} prefix={'VND '}/>
    return (
      <Container>
        <Range
        
          min={1000000}
          max={20000000}
          value={horizontal}
          format={format}
          onChange={this.handleChangeHorizontal}
          step={500000}
         
        />
        <div className="value">{format(horizontal)}</div>
      </Container>
    );
  }
}

export default Slide;
