import React, { Component } from "react";
import ImgPickerFood from "./ImagePicker/ImagePickerFood";
import styled from "styled-components";

const Container = styled.div`
  padding: 3rem 15%;
`;
const Caption = styled.h2`
 
`;
const Button = styled.button`
  padding: 1rem 2rem;
  margin: 0.5rem 1rem;
  border: 1px solid black;
  background: white;
  outline: none;
  &:hover {
    background: #81ecec;
  }
`;
const Content = styled.div`
  background: white;
  box-shadow: rgba(0, 0, 0, 0.3) 0px 0px 6px 0px;
`;
const Form = styled.form`
  padding: 1rem;
 
`;
class FoodForm extends Component {
  state = {
    food_subcategories:[],
  };
  saveAndContinue = (e) => {
    localStorage.setItem("basicform", JSON.stringify(this.state));
    e.preventDefault();
    this.props.nextStep();
  };

  back = (e) => {
    e.preventDefault();
    this.props.prevStep();
  };
  food = (food) => {
    let prevData = JSON.parse(localStorage.getItem("basicform"));
    this.setState({  ...prevData,food_subcategories: food.map(value => value.value) });
  };
  
  render() {
    return (
      <Container>
      <Caption>What Kind of Food Do You Like?</Caption>
        <Content>
          <Form>
            <ImgPickerFood values={this.food}></ImgPickerFood>
          </Form>
          <p style={{color:"red", fontStyle:"Italic",fontSize:"14px"}}>* You must select more than 5 kinds of food</p>
          <Button onClick={this.back}>Back</Button>
          <Button disabled={this.state.food_subcategories.length<5} onClick={this.saveAndContinue}>Continue </Button>
        </Content>
      </Container>
    );
  }
}

export default FoodForm;
