import React, { Component } from "react";
import ImgPickerStay from "./ImagePicker/ImagePickerStay";
import styled from "styled-components";

const Container = styled.div`
  padding: 3rem 15%;
`;

const Button = styled.button`
  padding: 1rem 2rem;
  margin: 0.5rem 1rem;
  border: 1px solid black;
  background: white;
  outline: none;
  &:hover {
    background: #81ecec;
  }
`;
const Caption = styled.h2`
 
`;
const Content = styled.div`
  background: white;
  box-shadow: rgba(0, 0, 0, 0.3) 0px 0px 6px 0px;
`;
const Form = styled.form`
  padding: 1rem;
`;
class PersonalDetails extends Component {
  state = {
    stay_subcategories: [],
  };
  saveAndContinue = (e) => {
    localStorage.setItem("basicform", JSON.stringify(this.state));
    e.preventDefault();
    this.props.nextStep();
  };

  back = (e) => {
    e.preventDefault();
    this.props.prevStep();
  };
  stay = (stay) => {
    let prevData = JSON.parse(localStorage.getItem("basicform"));
    let state = { ...prevData,stay_subcategories: [stay.value] }
    this.setState(state);
    localStorage.setItem("basicform", JSON.stringify(state));
  }
  render() {
    console.log('basic form state: ', this.state);
    return (
      <Container>
        <Caption>What Kind of Place Do You Like? </Caption>
        <Content>
          <Form>
            <ImgPickerStay values={this.stay}></ImgPickerStay>
          </Form>
          <p style={{color:"red", fontStyle:"Italic",fontSize:"14px"}}>* You must select one kind of place</p>
          <Button onClick={this.back}>Back</Button>
          <Button disabled={this.state.stay_subcategories.length==0} onClick={this.saveAndContinue}>Continue </Button>
        </Content>
      </Container>
    );
  }
}

export default PersonalDetails;
