import React from "react";
import moment from "moment";
import { Helmet } from "react-helmet";
import styled from "styled-components";
import DayPickerInput from "react-day-picker/DayPickerInput";
import "react-day-picker/lib/style.css";
import "./DateTime.css";
import { formatDate, parseDate } from "react-day-picker/moment";

const Container = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 1rem 2rem;
`;

const Input = styled(DayPickerInput)`
  & input {
    border-radius: 3px;
  }
`;
export default class DateTime extends React.Component {
  constructor(props) {
    super(props);
    this.handleFromChange = this.handleFromChange.bind(this);
    this.handleToChange = this.handleToChange.bind(this);
    this.state = {
      from: undefined,
      to: undefined,
    };
  }

  showFromMonth() {
    const { from, to } = this.state;
    if (!from) {
      return;
    }
    if (moment(to).diff(moment(from), "months") < 2) {
      this.to.getDayPicker().showMonth(from);
    }
  }

  handleFromChange(from) {
    console.log(from)
   
    // Change the from date and focus the "to" input field
    this.setState({ from });
    this.props["day_start"]({ from });//setState "to" to "day-end"
  }

  handleToChange(to) {
    console.log(to)
    this.setState({ to }, this.showFromMonth);
    this.props["day_end"]({ to }); //setState "to" to "day-end"
  }
  render() {
    const { from, to } = this.state;
    const modifiers = { start: from, end: to };
    let data = JSON.parse(localStorage.getItem("basicform"))||[];
    return (
      <Container>
        <Input
          value={moment(data.day_start).format("DD-MM-YYYY")}
          placeholder="From"
          format="DD-MM-YYYY"
          formatDate={formatDate}
          parseDate={parseDate}
          dayPickerProps={{
            selectedDays: [from, { from, to }],
            disabledDays: { before: new Date()},
            toMonth: to,
            modifiers,
            numberOfMonths: 2,
            onDayClick: () => this.to.getInput().focus(),
          }}
          onDayChange={this.handleFromChange}
        />{" "}
        --{" "}
       
          <Input
            ref={(el) => (this.to = el)}
            value={moment(data.day_end).format("DD-MM-YYYY")}
            placeholder="To"
            format="DD-MM-YYYY"
            formatDate={formatDate}
            parseDate={parseDate}
            dayPickerProps={{
              selectedDays: [from, { from, to }],
              disabledDays: { before: from, after: moment(from).add(3,'days').toDate()},
              modifiers,
              month: from,
              fromMonth: from,
              numberOfMonths: 2,
            }}
            onDayChange={this.handleToChange}
          />
        <Helmet></Helmet>
      </Container>
    );
  }
}
