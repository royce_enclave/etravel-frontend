import React, { Component } from "react";
import styled from "styled-components";
import { default as NumberFormat } from "react-number-format";
import moment from "moment";
import axios from "axios";
import { getToken, clearStorage } from "../../utils/Common";
import { Icon, Dimmer, Loader } from "semantic-ui-react";
import serverURL from "../../utils/Common";
import _ from "lodash";
import "./ConfirmForm.css";
import { Tag } from "antd";

const Container = styled.div`
  padding: 3rem 15%;
`;
const Content = styled.div`
  background: white;
  margin: 1rem 20%;
`;
const Caption = styled.h2`
  text-transform: capitalize;
`;
const Form = styled.form`
  padding: 1rem;
  box-shadow: rgba(0, 0, 0, 0.3) 0px 0px 6px 0px;
`;
const Button = styled.button`
  padding: 1rem 2rem;
  margin: 0.5rem 1rem;
  border: 1px solid black;
  background: white;
  outline: none;
  &:hover {
    background: #81ecec;
  }
`;
const Load = styled.div`
  height: 10rem;
  display: flex;
  justify-content: center;
  align-items: center;
  display: none;
`;

const config = {
  headers: {
    "auth-token": getToken(),
  },
};
class Confirmation extends Component {
  state = {};
  saveAndContinue = (e) => {
    e.preventDefault();
    let create = document.getElementById("load-create");
    create.style.display = "block";
    let prevData = JSON.parse(localStorage.getItem("basicform"));
    axios
      .post(
        `${serverURL}/trip/generate`,
        {
          number_of_day: prevData.number_of_day,
          name: prevData.name,
          budget: prevData.budget,
          date_start: prevData.day_start,
          date_end: prevData.day_end,
          kid: prevData.kid,
          adult: prevData.adult,
          stay_subcategories: prevData.stay_subcategories,
          food_subcategories: prevData.food_subcategories,
          special_subcategories: prevData.special_subcategories,
        },
        config
      )
      .then((response) => {
        console.log("will test: ", response.data);
        localStorage.setItem("generate", JSON.stringify(response.data.data));
        console.log(
          "Confirmation -> saveAndContinue -> response.data.data",
          response.data.data
        );

        this.props.nextStep();
      })
      .catch((error) => {
        alert("Something wrong. Try again!");
        window.location.href = "/trip/add";
        let basicForm = localStorage.getItem("basicform");
        clearStorage();
        localStorage.setItem("basicform", basicForm);
        console.log("Generate error: ", error.response.data.error);
        if (error.response)
          console.log("Generate Trip Error: ", error.response);
      });
  };

  back = (e) => {
    e.preventDefault();
    this.props.prevStep();
  };

  componentDidMount() {
    let basic = JSON.parse(localStorage.getItem("basicform"));
    this.setState(basic);
    let create = document.getElementById("load-create");
    create.disabled = true;
    axios
      .get(`${serverURL}/category`)
      .then((res) => {
        let listOfCategories = res.data.data;
        let listOfStayIds = basic.stay_subcategories;
        let listOfFoodIds = basic.food_subcategories;
        let listOfSpecialIds = basic.special_subcategories;

        let listOfFilteredByStay = _.remove(listOfCategories, (category) => {
          for (let item of listOfStayIds) if (item == category._id) return true;
        });
        let listOfFilteredByFood = _.remove(listOfCategories, (category) => {
          for (let item of listOfFoodIds) if (item == category._id) return true;
        });
        let listOfFilteredBySpecial = _.remove(listOfCategories, (category) => {
          for (let item of listOfSpecialIds)
            if (item == category._id) return true;
        });
        let stayName = listOfFilteredByStay.map((item) => item.name);
        let foodName = listOfFilteredByFood.map((item) => item.name);
        let specialName = listOfFilteredBySpecial.map((item) => item.name);
        this.setState({
          ...basic,
          stay_name: stayName,
          food_name: foodName,
          special_name: specialName,
        });
      })
      .catch((error) => console.log(error.response));
  }
  render() {
    console.log("state:", this.state);
    let stay_subcategories = this.state.stay_name ? this.state.stay_name : "";
    let food_subcategories = this.state.food_name ? this.state.food_name : "";
    let special_subcategories = this.state.special_name
      ? this.state.special_name
      : "";
    return (
      <Container>
        <Load id="load-create">
          <Dimmer active inverted>
            <Loader inverted>Creating...</Loader>
          </Dimmer>
        </Load>
        <Caption>Confirm your Details</Caption>
        <Content>
          <Form>
            <h4>
              Click Confirm if the following details have been correctly entered
            </h4>

            <div className="show-info">
              <div className="show-info-title">
                <h4>Trip Duration (days)</h4>
                <h4>Budget</h4>
                <h4>Start Date</h4>
                <h4>End Date</h4>
                <h4>Adult</h4>
                <h4>Kid</h4>
              </div>
              <div style={{ marginLeft: "10%" }}>
                {" "}
                <h4>{this.state.number_of_day}</h4>
                <h4>
                  <NumberFormat
                    style={{ fontWeight: "normal" }}
                    value={this.state.budget}
                    displayType={"text"}
                    thousandSeparator={true}
                    prefix="VND "
                  />
                </h4>
                <h4>{moment(this.state.day_start).format("DD/MM/YYYY")}</h4>
                <h4> {moment(this.state.day_end).format("DD/MM/YYYY")}</h4>
                <h4>{this.state.adult}</h4>
                <h4>{this.state.kid}</h4>
              </div>
            </div>
            <div>
            
              <div className="show-tagname">
                <h4>Stay</h4>
                <div
                  className="d-flex flex-wrap"
                  style={{ marginLeft: "15%", textTransform: "capitalize" }}
                >
                  {stay_subcategories
                    ? stay_subcategories.map((item) => (
                        <div className="mb-1">
                          <Tag  color="blue">{item} </Tag>
                        </div>
                      ))
                    : []}
                </div>
              </div>
              <div className="show-tagname">
                <h4>Foods</h4>
                <div
                  className="d-flex flex-wrap"
                  style={{ marginLeft: "11.5%", textTransform: "capitalize" }}
                >
                  {food_subcategories
                    ? food_subcategories.map((item) => (
                        <div className="mb-1">
                          <Tag color="blue">{item} </Tag>
                        </div>
                      ))
                    : []}
                </div>
              </div>
              <div className="show-tagname">
                <h4>Places</h4>
                <div
                  className="d-flex flex-wrap show-tag"
                  style={{ marginLeft: "10.5%", textTransform: "capitalize" }}
                >
                  {special_subcategories
                    ? special_subcategories.map((item) => (
                        <div className="mb-1">
                          <Tag color="blue">{item} </Tag>
                        </div>
                      ))
                    : []}
                </div>
              </div>
            </div>
            <br />
            <Button onClick={this.back}>Back</Button>
            <Button id="save" onClick={this.saveAndContinue}>
              Confirm
            </Button>
          </Form>
        </Content>
      </Container>
    );
  }
}

export default Confirmation;
