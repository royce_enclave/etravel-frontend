import React, { Component } from "react";
import axios from "axios";
import ImagePicker from "./Picker/react-image-picker";
import "./ImagePicker.css";
import { Map } from 'immutable';
import serverURL from '../../../utils/Common';

class ImagePickerFood extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listFoodCategories: [],
    };
    this.onPick = this.onPick.bind(this);
  }
  componentDidMount() {
    axios
      .get(`${serverURL}/category/main/5ea69cce3f3b270f908e2cb7`)
      .then((response) => {
        let listFoodCategories = response.data.data;
        let listFoodCategoriesFiltered = listFoodCategories.filter(item => !(item.name == "breakfast" || item.name == "lunch" || item.name == "dinner"))
        this.setState({
          listFoodCategories: listFoodCategoriesFiltered,
        });
      })
      .catch((error) => console.log(error));
  }
  onPick(image) {
    console.log(image);
    this.setState({ image });
    this.props.values(image);
    console.log(this.state);
  }

  render() {
    
    let prevData = JSON.parse(localStorage.getItem("basicform"));
    let foodCategories = prevData && prevData.food_subcategories ? prevData.food_subcategories : [];
    console.log('foodCategories', foodCategories);
    return (
      <div>
        <ImagePicker
          images={this.state.listFoodCategories.map((spot, _id) => ({
            src: spot.image,
            value: spot._id,
          }))}
          onPick={this.onPick}
          multiple
          selectImages = {foodCategories}
        />
      </div>
    );
  }
}

export default ImagePickerFood;
