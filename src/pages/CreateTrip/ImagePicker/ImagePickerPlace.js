import React, { Component } from "react";
import ImagePicker from "./Picker/react-image-picker";
import axios from "axios";
import serverURL from '../../../utils/Common';

class ImgPickerPlace extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listSpecialCategories: [],
    };
    this.onPick = this.onPick.bind(this);
  }
  componentDidMount() {
    axios
      .get(`${serverURL}/category/main/5ea69c8d3f3b270f908e2cb6`)
      .then((response) => {
        let listSpecialCategories = response.data.data;
        let listSpecialCategoriesFiltered = listSpecialCategories.filter(item => !(item.name == "morning" || item.name == "afternoon" || item.name == "night"))
        this.setState({
          listSpecialCategories:listSpecialCategoriesFiltered,
        });
        console.log("OK", response);
      })
      .catch((error) => console.log(error));
  }

  onPick(image) {
    this.setState({ image });
    this.props.values(image);
    console.log(this.state)
  }

  render() {
    let prevData = JSON.parse(localStorage.getItem("basicform"));
    let specialCategories = prevData && prevData.special_subcategories ? prevData.special_subcategories : [];
    return (
      <div>
        <ImagePicker
          images={this.state.listSpecialCategories.map((spot, _id) => ({
            src: spot.image,
            value: spot._id,
          }))}
          onPick={this.onPick}
          multiple
          selectImages = {specialCategories}
        />
      </div>
    );
  }
}

export default ImgPickerPlace;
