import React, { Component } from "react";
import axios from "axios";
import ImagePicker from "./Picker/react-image-picker";
import "./ImagePicker.css";
import serverURL from '../../../utils/Common';
class ImagePickerFood extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listspots: [],
    };
    this.onPick = this.onPick.bind(this);
  }
  componentDidMount() {
    axios
      .get(`${serverURL}/category/main/5ea69cda3f3b270f908e2cb8`)
      .then((response) => {
        this.setState({
          listspots: response.data.data,
        });
      })
      .catch((error) => console.log(error));
  }
  onPick(image) {
    this.setState({ image });
    this.props.values(image);
  }

  render() {
    let prevData = JSON.parse(localStorage.getItem("basicform"));
    let stayCategories = prevData && prevData.stay_subcategories ? prevData.stay_subcategories : [];
    return (
      <div>
        <ImagePicker
          images={this.state.listspots.map((spot, _id) => ({
            src: spot.image,
            value: spot._id,
          }))}
          onPick={this.onPick}
          selectImages = {stayCategories}
        />
      </div>
    );
  }
}

export default ImagePickerFood;
