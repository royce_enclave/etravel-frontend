import React, { Component } from "react";
import ImgPickerPlace from "./ImagePicker/ImagePickerPlace";
import styled from "styled-components";

import axios from "axios";
import { getToken } from "../../utils/Common";
const Container = styled.div`
  padding: 3rem 15%;
`;
const Caption = styled.h2`

`;
const Button = styled.button`
  padding: 1rem 2rem;
  margin: 0.5rem 1rem;
  border: 1px solid black;
  background: white;
  outline: none;  
  &:hover {
    background: #81ecec;
  }
`;
const Content = styled.div`
  background: white;
  box-shadow: rgba(0, 0, 0, 0.3) 0px 0px 6px 0px;
`;
const Form = styled.form`
  padding: 1rem;
`;
const config = {
  headers: {
    "auth-token": getToken(),
  },
};

class FoodForm extends Component {
  state = {
    special_subcategories: [],
  };
  saveAndContinue = (e) => {
    localStorage.setItem("basicform", JSON.stringify(this.state));
    e.preventDefault();
    this.props.nextStep();
    
  };

  back = (e) => {
    e.preventDefault();
    this.props.prevStep();
  };
  hobbies = (hobby) => {
    let prevData = JSON.parse(localStorage.getItem("basicform"));
    this.setState({...prevData,
      special_subcategories: hobby.map((value) => value.value)
      
    });
  };
  
  render() {
    return (
      <Container>
      <Caption>Which Place You Want To Go?</Caption>
        <Content>
          <Form>
            <ImgPickerPlace values={this.hobbies}></ImgPickerPlace>
          </Form>
          <p style={{color:"red", fontStyle:"Italic",fontSize:"14px"}}>* You must select more than 5 kinds of hobbies</p>
          <Button onClick={this.back}>Back</Button>
          <Button disabled={this.state.special_subcategories.length<5} onClick={this.saveAndContinue}>Continue </Button>
          
        </Content>
      </Container>
    );
  }
}

export default FoodForm;
