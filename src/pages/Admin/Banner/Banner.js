import React from "react";
import PropTypes from "prop-types";
import "./Banner.scss";
import { useHistory, Link } from "react-router-dom";

import Logo from "../../../components/Header/logo.png";

const Banner = (props) => {
  let history = useHistory();
  return (
    <div className="admin-banner d-flex align-items-center justify-content-between">
      <div className="admin-banner--logo">
        <div
          className="admin-banner--logo-img_wrapper"
          style={{
            height: "73px",
            width: "200px",
            backgroundImage: `url(${Logo})`,
            backgroundPosition: "center",
            backgroundSize: "cover",
            backgroundRepeat: "no-repeat",
          }}
        ></div>
      </div>
      <div
        className="btn-logout d-flex align-items-center"
        style={{ height: "73px" }}
      >
        <Link className="px-4 py-2 border border-radius mr-4" to="/admin/login">
          Logout
        </Link>
      </div>
    </div>
  );
};

Banner.propTypes = {};

export default Banner;
