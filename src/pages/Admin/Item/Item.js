import React from "react";
import PropTypes from "prop-types";

const Item = (props) => {
  const { item } = props;
  return (
    <tr>
      <th scope="row">{item._id}</th>
      <td>{item.name}</td>
      <td>{item.address}</td>
      <td>{item.price}</td>
      <td>{item.comments?.length}</td>
    </tr>
  );
};

Item.propTypes = {};

export default Item;
