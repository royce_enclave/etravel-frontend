import React from "react";
import PropTypes from "prop-types";

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
} from "react-router-dom";

import "./Admin.scss";

import Spot from "./functionals/Spot/Spot";
import Category from "./functionals/Category/Category";
import Banner from "./Banner/Banner";
import Header from "./Header/Header";
import DashBoard from "./DashBoard/DashBoard";
import Login from "./Login/Login";
import PrivateRoute from "./PrivateRoute/PrivateRoute";

const Admin = (props) => {
  const { path, url } = useRouteMatch();

  return (
    <Switch>
      <Route path={`${path}/login`} component={Login} />
      <div className="admin-wrapper">
        <div className="admin row">
          <Banner />
          <Header url={url} />
          <PrivateRoute exact path={path} component={DashBoard}></PrivateRoute>
          <PrivateRoute path={`${path}/spots`} children={<Spot />} />
          <PrivateRoute path={`${path}/categories`} children={<Category />} />
        </div>
      </div>
    </Switch>
  );
};

Admin.propTypes = {};

export default Admin;
