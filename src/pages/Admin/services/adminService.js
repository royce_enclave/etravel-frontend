import axios from "axios";

const urlLogin = "http://54.169.72.199:5000/admin/login";

const getAuth = () => {
  return localStorage.getItem("admin-user");
};

const login = async (username, password) => {
  try {
    const data = { username: username, password: password };
    const response = await axios.post(urlLogin, data);
    if (response.data.code == 200) {
      localStorage.setItem("admin-user", response.data.data.token);
      return { result: true, message: "Login success" };
    }
  } catch (err) {
    return { result: false, message: err.message };
  }
};

const adminService = {
  login,
  getAuth,
};

export default adminService;
