import React, { useState, useEffect, useRef } from "react";
import PropTypes from "prop-types";
import { Table, Tag, Popconfirm, Modal } from "antd";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCheck,
  faTimes,
  faTrash,
  faClipboardCheck,
  faTimesCircle,
} from "@fortawesome/free-solid-svg-icons";
import ExpandableRecord from "./ExpandableRecord";
import _ from "lodash";

const DataTable = (props) => {
  const { items, setItems, onDeleteItem, onChangeVerify } = props;
  const [data, setData] = useState([]);

  const ref = useRef(null);

  const compareTwoStrings = (a, b) => {
    if (a > b) return 1;
    if (a < b) return -1;
    return 0;
  };
  useEffect(() => {
    const datas = items.map((item) => {
      const category = { ...item.category };
      const categories = [];
      for (let item in category.sub_categories) {
        categories.push(category.sub_categories[item]);
      }

      const datas = {
        key: item._id,
        main_category: category,
        sub_categories: categories,
        ...item,
      };

      return datas;
    });
    setData(datas);
  }, [items]);

  const columns = [
    {
      title: "name",
      dataIndex: "name",
      key: "name",
      width: "33%",
      sorter: (a, b) => compareTwoStrings(a.name, b.name),
    },
    {
      title: "verify",
      dataIndex: "verify",
      key: "verify",
      width: "5.3%",
      defaultSortOrder: "ascend",
      sorter: (a, b) => a.verify > b.verify,
      render: (verify) => (
        <span>
          <FontAwesomeIcon
            color={verify ? "green" : "red"}
            icon={verify ? faCheck : faTimes}
          />
        </span>
      ),
    },
    {
      title: "main category",
      dataIndex: "main_category",
      key: "main_category",
      width: "8.3%",
      render: (item) => <p>{item.name}</p>,
    },
    {
      title: "sub categories",
      dataIndex: "sub_categories",
      key: "sub_categories",
      width: "25.3%",
      render: (items) => (
        <div className="d-flex flex-wrap">
          {items.map((item) => (
            <Tag color="blue" className="mb-1">
              {item.name}
            </Tag>
          ))}
        </div>
      ),
    },
    { title: "price", dataIndex: "price", key: "price", width: "8.3%" },
    { title: "rating", dataIndex: "rating", key: "rating", width: "8.3%" },
    {
      title: "time duration",
      dataIndex: "time_duration",
      key: "time_duration",
      width: "8.3%",
    },
    {
      title: "action",
      dataIndex: "action",
      width: "3%",
      render: (text, record) => (
        <div
          className="row flex-wrap justify-content-around"
          style={{ fontSize: "30px" }}
        >
          <Popconfirm
            title="change verify?"
            onConfirm={() => onChangeVerify(record.key, !record.verify)}
          >
            <FontAwesomeIcon color="blue" icon={faClipboardCheck} />
          </Popconfirm>
          <Popconfirm
            title="Sure to delete?"
            onConfirm={() => onDeleteItem(record.key)}
          >
            <FontAwesomeIcon color="red" icon={faTimesCircle} />
          </Popconfirm>
        </div>
      ),
    },
  ];

  const expandable = {
    expandedRowRender: (record) => <ExpandableRecord record={record} />,
  };

  return (
    <div className="content-table col-12">
      <Table
        columns={columns}
        dataSource={data}
        expandable={expandable}
      ></Table>
    </div>
  );
};

DataTable.propTypes = {};

export default DataTable;
