import React from "react";
import PropTypes from "prop-types";

const ExpandableRecord = (props) => {
  const { record } = props;
  return (
    <div className="table-item--detail row ml-4">
      <div className="record-img--wrapper col-3" style={{ height: "200px" }}>
        <div
          className="record-img"
          style={{
            backgroundImage: `url(${record.cover_image})`,
            backgroundSize: "cover",
            paddingTop: "100%",
            backgroundRepeat: "no-repeat",
            backgroundPosition: "center",
          }}
        ></div>
      </div>
      <div className="record-detail col-9">
        <div className="record-id col-12 row">
          <div className="col-2 font-weight-bold">ID </div>
          <div className="col-10">{record.key}</div>
        </div>
        <div className="record-address col-12 row">
          <div className="col-2 font-weight-bold">Address </div>
          <div className="col-10">{record.address}</div>
        </div>
        <div className="record-description col-12 row">
          <div className="col-2 font-weight-bold">Description </div>
          <div className="col-10">{record.description}</div>
        </div>
        <div className="record-auth col-12 row">
          <div className="col-2 font-weight-bold">Author </div>
          <div className="col-10">{record.author.full_name}</div>
        </div>
        <div className="record-vido col-12 row">
          <div className="col-2 font-weight-bold">Latitude </div>
          <div className="col-10">{record.latitude}</div>
        </div>
        <div className="record-longitude col-12 row">
          <div className="col-2 font-weight-bold">Longitude </div>
          <div className="col-10">{record.longtitude}</div>
        </div>
        <div className="record-img col-12 row align-items-center">
          <div className="col-2 font-weight-bold">Images</div>
          <div className="list-images col-10 d-flex flex-wrap">
            {record.images.map((image) => (
              <div
                className="img-wrapper m-2"
                style={{ width: "100px", height: "75px" }}
              >
                <div
                  className="img"
                  style={{
                    backgroundImage: `url(${image})`,
                    backgroundSize: "cover",
                    paddingTop: "75%",
                    backgroundRepeat: "no-repeat",
                    backgroundPosition: "center",
                  }}
                ></div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

ExpandableRecord.propTypes = {};

export default ExpandableRecord;
