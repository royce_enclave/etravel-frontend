import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import axios from "axios";
import DataTable from "./DataTable/DataTable";
import { useParams } from "react-router-dom";

import "./Spot.scss";
import adminService from "../../services/adminService";

const Spot = (props) => {
  const [items, setItems] = useState([{}]);

  const { type } = useParams();

  const url = `http://54.169.72.199:5000/admin/spots`;
  const options = {
    headers: {
      "auth-token": adminService.getAuth(),
    },
  };

  const fetchData = async () => {
    setItems([]);
    try {
      const response = await axios(url, options);
      console.log(response.data.data);
      if (response.data.code == 200) {
        setItems(response.data.data);
      } else {
        console.log(response.data.message);
      }
    } catch (err) {
      console.log(err.response);
    }
  };

  useEffect(() => {
    fetchData();
  }, [type]);

  const onDeleteItem = async (id) => {
    try {
      const response = await axios.delete(
        `http://54.169.72.199:5000/admin/spots/${id}`,
        options
      );
      if (response.data.code == 200) {
        setItems(items.filter((item) => item._id != id));
      }
    } catch (err) {
      console.log(err.message);
    }
  };

  const onChangeVerify = async (id, verify) => {
    console.log(id, verify);
    try {
      const response = await axios.put(
        `http://54.169.72.199:5000/admin/spots/verify/${id}`,
        { verify: verify },
        options
      );
      console.log(response);
      if (response.data.code == 200) {
        setItems(
          items.map((item) => {
            if (item._id == id) {
              item.verify = !item.verify;
            }
            return item;
          })
        );
      }
    } catch (err) {
      console.log(err.response);
    }
  };

  return (
    <div className="container col-12 col-lg-10 height-100 overflowy-scroll">
      <div className="ng_content--header d-flex justify-content-between row col-12">
        <div className="title col-6 d-flex align-items-center">
          <span className="text-bold"> {type} </span>|| total record :{" "}
          {items.length}
        </div>
      </div>
      <DataTable
        items={items}
        onDeleteItem={onDeleteItem}
        onChangeVerify={onChangeVerify}
        setItems={setItems}
      />
    </div>
  );
};

Spot.propTypes = {};

export default Spot;
