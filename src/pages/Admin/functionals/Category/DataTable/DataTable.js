import React from "react";
import PropTypes from "prop-types";
import { Table, Tag } from "antd";

const DataTable = (props) => {
  const { categories } = props;
  const columns = [
    { title: "ID", key: "_id", dataIndex: "_id" },
    { title: "name", key: "name", dataIndex: "name" },
    {
      title: "Sub Categories",
      key: "sub_categories",
      dataIndex: "sub_categories",
      width: "60%",
      render: (text, record) => (
        <div className="d-flex flex-wrap">
          {record.sub_categories.map((sub_category) => (
            <div className="mb-1">
              <Tag color="blue">{sub_category.name} </Tag>
            </div>
          ))}
        </div>
      ),
    },
  ];
  return (
    <div className="col-12">
      <Table columns={columns} dataSource={categories} />
    </div>
  );
};

DataTable.propTypes = {};

export default DataTable;
