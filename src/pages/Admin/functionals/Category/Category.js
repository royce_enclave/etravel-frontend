import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import axios from "axios";

import DataTable from "./DataTable/DataTable";
import adminService from "../../services/adminService";

const configs = {
  headers: { "auth-token": adminService.getAuth() },
};

const Cateogory = (props) => {
  const [categories, setCategories] = useState([]);

  const url = "http://54.169.72.199:5000/admin/category/main";

  const getSubCategories = (url) => {
    return new Promise((resolve, reject) => {
      axios(url, configs)
        .then((data) => resolve(data))
        .catch((err) => reject(err));
    });
  };

  const getAllCategories = async () => {
    try {
      const mainCategories = await axios(url, configs);
      const items = mainCategories.data.data;
      const subPromises = await items.map((item) =>
        getSubCategories(`http://54.169.72.199:5000/category/main/${item._id}`)
      );

      Promise.all(subPromises)
        .then((values) => {
          let newList = items.map((item, index) => {
            const newItem = {
              ...item,
              sub_categories: values[index].data.data,
            };
            return newItem;
          });
          setCategories(newList);
        })
        .catch((err) => console.log(err));
    } catch (err) {
      console.log(err.response);
    }
  };

  useEffect(() => {
    getAllCategories();
  }, []);

  return (
    <div className="container col-12 col-lg-10 height-100 overflowy-scroll">
      <div className="ng_content--header d-flex justify-content-between row col-12">
        <div className="title col-6 d-flex align-items-center">
          <span className="text-bold"> Categories </span>|| total record :{" "}
          {categories.length}
        </div>
      </div>
      <DataTable categories={categories} />
    </div>
  );
};

Cateogory.propTypes = {};

export default Cateogory;
