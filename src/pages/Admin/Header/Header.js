import React from "react";
import PropTypes from "prop-types";

import "./Header.scss";

import { Link } from "react-router-dom";
const Header = (props) => {
  let { url } = props;
  return (
    <div className="admin-header col-12 col-lg-2 ">
      <div className="admin-header--wrapper d-flex flex-column justify-content-start align-items-start">
        <Link exact to={`${url}`}>
          Dash Board
        </Link>
        <Link exact to={`${url}/users`}>
          User
        </Link>
        <Link exact to={`${url}/spots`}>
          Spot
        </Link>
        <Link to={`${url}/trips`}> Trip</Link>
        <Link to={`${url}/categories`}> Category</Link>
      </div>
    </div>
  );
};

Header.propTypes = {};

export default Header;
