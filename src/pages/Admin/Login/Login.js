import React from "react";
import PropTypes from "prop-types";
import { Input, Checkbox, Button, Form } from "antd";
import { useHistory, useLocation } from "react-router-dom";
import "./Login.scss";

import adminService from "../services/adminService";
import logoImg from "./logo.png";

const Login = (props) => {
  let history = useHistory();
  let location = useLocation();

  localStorage.clear();

  const onSubmit = async (values) => {
    const result = await adminService.login(values.username, values.password);

    if (result) {
      let { from } = location.state || { from: { pathname: "/admin" } };
      history.push(from);
    }
  };
  return (
    <div className="login-wrapper d-flex justify-content-center align-items-center">
      <div className="login-img" />
      <div className="login-form row col-12 col-lg-6 m-auto justify-content-center">
        <div className="login-form--img_wrapper" />
        <div className="login-app_title">eTravel</div>
        <div className="login-content col-12">
          <Form className="col-12 col-lg-6 m-auto" onFinish={onSubmit}>
            <Form.Item
              label="Username"
              name="username"
              rules={[
                { required: true, message: "Please input your username" },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Password"
              name="password"
              rules={[
                { required: true, message: "Please input your password" },
              ]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item>
              <Button type="primary" htmlType="submit">
                Login
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
};

Login.propTypes = {};

export default Login;
