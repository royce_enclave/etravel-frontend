import React, { Component } from 'react';
import axios from 'axios';
import { getToken, getUser } from '../../utils/Common';
import './AddLocation.css';
import MapView from '../../components/Google-map/Home';
import 'bootstrap/dist/css/bootstrap.min.css';
import Select from 'react-select';
import UploadImage from '../../components/UploadImageAdd/UploadImage';
import serverURL from '../../utils/Common';

const user = getUser();
class AddLocation extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
      address: '',
      city: '',
      district: '',
      street: '',
      description: '',
      price: 0,
      latitude: 0,
      longtitude: 0,
      time_duration: 0,
      author: '',
      main_category: '',
      sub_categories: [],
      cover_image: '',
      images: [],
      selectedOption: '',
      selectedOption2: [],
      selectedOption3: [],
    };
    this.handleChange1 = this.handleChange1.bind(this);
    this.handleChange2 = this.handleChange2.bind(this);
    this.handleChange3 = this.handleChange3.bind(this);
    this.name = this.name.bind(this);
    this.address = this.address.bind(this);
    this.city = this.city.bind(this);
    this.district = this.district.bind(this);
    this.street = this.street.bind(this);
    this.description = this.description.bind(this);
    this.price = this.price.bind(this);
    this.time_duration = this.time_duration.bind(this);
    this.cover_image = this.cover_image.bind(this);
    this.images = this.images.bind(this);
    this.author = this.author.bind(this);
    this.main_category = this.main_category.bind(this);
    this.sub_categories = this.sub_categories.bind(this);
    this.longtitude = this.longtitude.bind(this);
    this.latitude = this.latitude.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  name(event) {
    this.setState({ name: event.target.value });
  }
  address(event) {
    this.setState({ address: event.target.value });
  }
  city(event) {
    this.setState({ city: event.target.value });
  }
  district(event) {
    this.setState({ district: event.target.value });
  }
  street(event) {
    this.setState({ street: event.target.value });
  }
  description(event) {
    this.setState({ description: event.target.value });
  }
  price(event) {
    this.setState({ price: event.target.value });
  }
  cover_image(event) {
    this.setState({ cover_image: event.target.value });
  }
  time_duration(event) {
    this.setState({ time_duration: event.target.value });
  }
  images(event) {
    this.setState({ images: event.target.value });
  }
  author(event) {
    this.setState({ author: event.target.value });
  }
  main_category(event) {
    this.setState({ main_category: event.target.value });
  }
  sub_categories(event) {
    this.setState({ sub_categories: event.target.value });
  }
  longtitude(event) {
    this.setState({ longtitude: event.target.value });
  }
  latitude(event) {
    this.setState({ latitude: event.target.value });
  }
  handleChange(event) {
    this.setState({ value: event.target.value });
  }
  handleChange1 = (selectedOption) => {
    this.setState({ main_category: selectedOption.value });
  };
  handleChange2 = (selectedOption2) => {
    selectedOption2 = selectedOption2 ? selectedOption2 : []; //Check trường hợp rỗng (null)
    let arrayOfSelected = selectedOption2.map((element) => element.value);
    let session = this.state.session ? this.state.session : [];
    let result = session.concat(arrayOfSelected);
    this.setState({
      temp_sub_categories: arrayOfSelected,
      sub_categories: result,
    });
  };
  handleChange3 = (selectedOption3) => {
    let arrayOfSubCategories = this.state.temp_sub_categories;
    selectedOption3 = selectedOption3 ? selectedOption3 : []; //Check trường hợp rỗng (null)
    let arrayOfSession = selectedOption3.map((element) => element.value)
      ? selectedOption3.map((element) => element.value)
      : [];

    let result = arrayOfSession.concat(arrayOfSubCategories);
    this.setState({
      session: arrayOfSession,
      sub_categories: result,
    });
  };
  isValidation() {
    if (this.state.price < 0 || this.state.price > 50000000) {
      alert('Price is incorrect!');
      return false;
    } else if (this.state.time_duration < 0 || this.state.time_duration > 360) {
      alert('Time duration is incorrect');
      return false;
    } else if (this.state.name == '') {
      alert('Name is required!');
      return false;
    } else if (
      this.state.main_category == '' ||
      this.state.sub_categories.length == 0
    ) {
      alert('Category is required!');
      return false;
    } else if (this.state.latitude == 0 || this.state.longtitude == 0) {
      alert(`You must select your spot's location`);
      return false;
    }
    return true;
  }
  handleSubmit(event) {
    var config = {
      headers: {
        'auth-token': getToken(),
      },
    };
    if (this.isValidation()) {
      axios
        .post(
          `${serverURL}/spot`,
          {
            name: this.state.name,
            main_category: this.state.main_category,
            sub_categories: this.state.sub_categories,
            description: this.state.description,
            price: this.state.price,
            time_duration: this.state.time_duration,
            cover_image: this.state.cover_image,
            images: this.state.images,
            author: user._id,
            longtitude: this.state.longtitude,
            latitude: this.state.latitude,
            address: this.state.address,
            city: this.state.city,
            district: this.state.district,
            street: this.state.street,
          },
          config
        )
        .then((response) => {
          alert('Add spot sucessfull');
          window.location.href = '/info/spots-profile';
        })
        .catch((error) => {
          alert('Try Again, something wrong!');
          console.log(error.response);
        });
    }
  }

  dataa = (dt) => {
    this.setState({
      ...this.state,
      latitude: dt.newLat,
      longtitude: dt.newLng,
      address: dt.fullAddress,
      city: dt.city,
      district: dt.district,
      street: dt.street,
    });
  };
  dataSelect = (dts) => {
    this.setState({
      ...this.state,
      main_category: dts.selectedOption.value,
    });
  };
  dataSelect2 = (dts) => {
    this.setState({
      ...this.state,
      sub_categories: dts.selectedOption2.value,
    });
  };
  dataImage = (dt_img) => {
    this.setState({
      ...this.state,
      images: dt_img,
      cover_image: dt_img[0],
    });
  };
  componentDidMount() {
    axios
      .get(`${window.$serverURL}/category`)
      .then((response) => {
        this.setState({
          optionsSub: response.data.data,
        });
      })
      .catch((error) => console.log(error));
  }
  render() {
    let optionSub = this.state.optionsSub ? this.state.optionsSub : [];

    let main_categories = [];
    let special_session = [];
    let food_meal_session = [];
    let categoryFilterByMain = [];
    main_categories = optionSub.filter((item) => !item.parent);

    special_session = optionSub.filter(
      (item) =>
        item.name.toLowerCase() == 'morning' ||
        item.name.toLowerCase() == 'afternoon' ||
        item.name.toLowerCase() == 'night'
    );
    //optionSub => special => subOfSpecial => remove special_session = sub_special
    categoryFilterByMain = optionSub.filter(
      (item) => !main_categories.includes(item)
    );

    let subOfSpecial = categoryFilterByMain.filter(
      (item) => item.parent.name.toLowerCase() === 'special'
    );

    subOfSpecial = subOfSpecial.filter(
      (item) => !special_session.includes(item)
    );

    //optionSub => food => subOfFood => remove food_meal = sub_Food
    food_meal_session = optionSub.filter(
      (item) =>
        item.name.toLowerCase() === 'breakfast' ||
        item.name.toLowerCase() === 'lunch' ||
        item.name.toLowerCase() === 'dinner'
    );
    let subOfFood = categoryFilterByMain.filter(
      (item) => item.parent.name.toLowerCase() === 'food'
    );

    subOfFood = subOfFood.filter((item) => !food_meal_session.includes(item));

    let subOfStay = categoryFilterByMain.filter(
      (item) => item.parent.name.toLowerCase() === 'stay'
    );

    //
    let main_categories_options = [];
    let sub_session_options = [];
    let sub_categories_options = [];
    let session = food_meal_session.concat(special_session);
    categoryFilterByMain = categoryFilterByMain.filter(
      (item) => !session.includes(item)
    );

    //label, value (key)
    for (let item of main_categories) {
      let label = item.name;
      let value = item._id;
      main_categories_options.push({ label: label, value: value });
    }

    //label, value, link (key)

    for (let item of categoryFilterByMain) {
      let label = item.name;
      let value = item._id;
      let link = item.parent._id;
      sub_categories_options.push({
        label: label,
        value: value,
        link: link,
      });
    }

    for (let item of special_session) {
      let label = item.name;
      let value = item._id;
      let link = item.parent._id;
      sub_session_options.push({
        label: label,
        value: value,
        link: link,
      });
    }
    for (let item of food_meal_session) {
      let label = item.name;
      let value = item._id;
      let link = item.parent._id;
      sub_session_options.push({
        label: label,
        value: value,
        link: link,
      });
    }

    const sub_categories_options_filtered = sub_categories_options.filter(
      (o) => o.link === this.state.main_category
    );
    const sub_session_options_filtered = sub_session_options.filter(
      (o) => o.link === this.state.main_category
    );
    // console.log("test state: ", this.state)
    return (
      <div className="content-add">
        <div className="header-add">Add New Spot</div>
        <form className="form-add">
          <div className="form-group">
            <label>
              Name<strong style={{ color: 'red' }}>*</strong>
            </label>
            <input
              autoFocus
              type="text"
              className="form-controls"
              placeholder="Input name of location"
              name="name"
              onChange={this.name}
            />
          </div>
          <div className="form-group">
            <label>
              Category <strong style={{ color: 'red' }}>*</strong>
            </label>
            <Select
              style={{ height: '20px' }}
              name="name"
              value={this.state.selectedOption.value}
              onChange={this.handleChange1}
              options={main_categories_options}
            />
          </div>

          <div className="form-group">
            <label>
              Sub Categories<strong style={{ color: 'red' }}>*</strong>
            </label>
            <h6 style={{ marginTop: '10px' }}>Select Day Session </h6>
            <Select
              isMulti="true"
              name="name"
              value={this.state.selectedOption3.value}
              onChange={this.handleChange3}
              options={sub_session_options_filtered}
            />
            <h6 style={{ marginTop: '10px' }}>Select sub categories </h6>
            <Select
              isMulti="true"
              name="name"
              value={this.state.selectedOption2.value}
              onChange={this.handleChange2}
              options={sub_categories_options_filtered}
            />
          </div>
          <div className="form-group">
            <label>Description</label>
            <textarea
              type="text"
              className="form-controls"
              placeholder="Input some description"
              description="description"
              onChange={this.description}
            />
          </div>
          <div style={{ display: 'flex' }}>
            <div className="form-group">
              <label>Cost (VND/person)</label>
              <br />
              <input
                type="number"
                className="form-controls"
                placeholder="Input price"
                time_duration="time_duration"
                onChange={this.price}
                step={10000}
                min={0}
                max={1000000}
              />
            </div>
            <div className="form-group" style={{ marginLeft: '2.5%' }}>
              <label>Time Duration (Minutes)</label>
              <input
                type="number"
                className="form-controls"
                placeholder="Input time duration"
                time_duration="time_duration"
                onChange={this.time_duration}
                step={30}
                min={0}
                max={720}
              />
            </div>
          </div>

          <div className="form-group">
            <div
              style={{
                width: '100%',
                paddingTop: '15px',
              }}
            >
              <MapView data={this.dataa}></MapView>
            </div>
          </div>
          <div className="form-group">
            <label>
              Images <strong style={{ color: 'red' }}>*</strong>
            </label>
            <UploadImage data={this.dataImage}></UploadImage>
          </div>

          <button
            type="button"
            className="button-add button1-add"
            onClick={this.handleSubmit}
          >
            Add
          </button>
        </form>
      </div>
    );
  }
}
export default AddLocation;
