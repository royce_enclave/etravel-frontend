import React from "react";
import styled from "styled-components";
import { Draggable } from "react-beautiful-dnd";
import Popup from "reactjs-popup";

import { MdNearMe } from "react-icons/md";
import { FaCarAlt, FaCaretDown } from "react-icons/fa";

import { TiDeleteOutline } from "react-icons/ti";
import PopUps from "./PopUp";
import initialData from "./InitialData";

const Car = styled(FaCarAlt)`
  margin: 0.35rem 0.5rem 0 -0.2rem;
  color: #212121;
`;

const Moving = styled.div`
  position: absolute;
  display: flex;
  left: 4.7rem;
  top: -7%;
  z-index: 99;
  ::before {
    content: "";
    font-size: 12px;
    font-weight: 700;
  }
`;

const Card = styled.div`
  position: relative;
  /* border-radius: 8px; */
  padding: 3rem 1rem;
  background: ${(props) => (props.isDragging ? "lightgreen" : "white")};
  max-width: 1440px;
  min-width: 456px;
  &::before {
    content: "";
    position: absolute;
    top: 0.5rem;
    left: 5rem;
    width: 1px;
    height: 2.5rem;
    background: black;
    transform: translateX(-50%);
  }
  &:first-child {
    &:before {
      display: none;
    }
    & > ${Moving} {
    display: none;
  }
  }
  &::after {
    content: "";
    position: absolute;
    left: 5rem;
    width: 1px;
    height: 2.5rem;
    background: black;
    transform: translateX(-50%);
  }
  :last-child::after {
    content: none;
  }
  /* :last-child > ${Moving} {
    display: none;
  } */
  :active::before {
    content: none;
  }
  :active::after {
    content: none;
  }
  :active > ${Moving} {
    display: none;
  }
  :active + :nth-child(2):before {
    display: none;
  }
`;

const Top = styled.div`
  display: flex;
  margin-right: 0.5rem;
  padding-right: 0.5rem;
`;

const Name = styled.div`
  padding: 0.5rem 1rem;
`;

const Bottom = styled.div`
  display: flex;
  padding: 0.5rem 0;
  margin-right: 1.5rem;
`;

const Title = styled.h6`
  display: block;
  font-size: 1.5rem;
  text-align: start;
`;

const Nearby = styled.div`
  margin-left: auto;
  color: #0092ff;
`;

const Image = styled.img`
  width: 7rem;
  height: 6rem;
  /* border-top-left-radius: 8px; */
`;

const TimeStart = styled.div`
  width: 6rem;
  text-align: center;
  color: #0092ff;
`;

const TimeDuration = styled.span`
  margin-left: 0.4rem;
  color: #0092ff;
`;

export default class Spot extends React.Component {
  // state = initialData;
  /**
   * send Id spot to removeSpot (column.js)
   */
  removeSpot = (id) => {
    this.props.removeSpot(id);
  };
  /**
   * send Id spot to addSpot (column.js)
   */
  addSpot = (newSpot) => {
    const spotIndex = this.props.index;
    this.props.addSpot({ newSpot, spotIndex });
  };
  render() {
    const formatter = new Intl.NumberFormat('ja-JP', {
      minimumFractionDigits: 0
    })
    const { spot } = this.props;

    let timeDuration = spot["time_duration"];
    let hourDuration = Math.floor(timeDuration / 60);
    let minuteDuration = timeDuration - hourDuration * 60;

    let currentTime = Math.round(spot["current_time"]);
    let hourStart = Math.floor(currentTime / 60);
    let minuteStart = currentTime - hourStart * 60;

    let distance = Math.round(spot.distance * 100) / 100;
    return (
      <Draggable draggableId={spot._id} index={this.props.index} isDragDisabled={this.props.index === 0}>
        {(provided, snapshot) => (
          <Card
            {...provided.draggableProps}
            {...provided.dragHandleProps}
            ref={provided.innerRef}
            isDragging={snapshot.isDragging}
            isDragDisabled={this.props.index === 0}
          >
            <Moving>
              <Car />
              <div>
                {<FaCaretDown />}
                {distance}km
              </div>
            </Moving>
            <div
              style={{
                background: "#ffffff",
                // borderRadius: 8,
                boxShadow: "0px 1px 5px 0px rgba(0,0,0,0.5)",
              }}
            >
              <Top>
                <Image src={spot["cover_image"]} alt=""></Image>
                <Name>
                  <Title>{spot.name} </Title>
                  <div style={{ color: "rgba(0,0,0,.4)", textAlign: "start" }}>
                    Time Duration:{" "}
                    <TimeDuration>
                      {timeDuration < 59
                        ? timeDuration + "m"
                        : `${hourDuration +
                        "h" +
                        (minuteDuration > 0 ? minuteDuration + "m" : "")}`}
                    </TimeDuration>
                    <div >Cost:  <TimeDuration>~VND {formatter.format(spot.price)}</TimeDuration> </div>
                  </div>
                </Name>
                {this.props.index !== 0 ? (
                  <div
                    style={{
                      marginLeft: "auto",
                      paddingTop: "1rem",
                      color: "rgba(0,0,0,.4)",
                    }}
                    onClick={() => this.removeSpot(spot._id)}
                  >
                    <TiDeleteOutline style={{ fontSize: 24 }} />
                  </div>
                ) : (
                    <div></div>
                  )}
              </Top>
              <Bottom>
                <TimeStart>
                  <span className="time-start">
                    {`${(hourStart < 10 ? "0" + hourStart : hourStart) +
                      ":" +
                      (minuteStart < 10 ? "0" + minuteStart : minuteStart)}`}
                  </span>
                </TimeStart>
                <Popup
                  trigger={
                    <Nearby>
                      <MdNearMe />
                      <span>Near by</span>
                    </Nearby>
                  }
                  modal
                  closeOnDocumentClick
                >
                  {(close) => (
                    <PopUps
                      addSpot={this.addSpot}
                      spotId={spot._id}
                      listSpotIds={this.props.listSpotIds}
                      close={close}
                    />
                  )}
                </Popup>
              </Bottom>
            </div>
            {/* <Moving>
              <Car />
              <div>
                {<FaCaretDown />}
                {distance}km
              </div>
            </Moving> */}
          </Card>
        )}
      </Draggable>
    );
  }
}
