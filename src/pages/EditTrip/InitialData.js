import _ from "lodash";

const initialData = {
  name: "Trip Danang 2",
  sub_categories: ["5e6608f38f8c030008343d18", "5e7c6b2e61a838416c24e57b"],
  date_start: "2020/04/14",
  date_end: "2020/04/15",
  budget: 13000,
  cover_image:
    "https://live.staticflickr.com/65535/49646896796_6e7611ebec_c.jpg",
  creator: "5e81918ddeeff0201812714d",
  schedules: [
    {
      time_start: 480,
      time_end: 1318.8933333333332,
      length: 15,
      spots: [
        {
          _id: "5e81ae45deeff02018127165",
          name: "Bún chả cá",
          verify: false,
          category: {
            _id: "5e6894a73e20aa00087fe8a6",
            name: "Food",
            sub_categories: [
              {
                _id: "5e81639d0f0e550cf4940627",
                name: "Breakfast",
              },
              {
                _id: "5e7c6d2361a838416c24e586",
                name: "Noodle",
              },
            ],
          },
          author: {
            _id: "5e81918ddeeff0201812714d",
          },
          description: "109 Nguyễn Chí Thanh, quận Hải Châu",
          price: 40000,
          latitude: 16.074149,
          longtitude: 108.220844,
          time_duration: 45,
          cover_image:
            "https://sites.google.com/site/bunchaca109nguyenchithanh/_/rsrc/1439382388182/home/foody-bun-cha-ca-nguyen-chi-thanh-74283-635346517614605000.JPG",
          images: [
            "https://images.foody.vn/res/g1/5742/prof/s1242x600/foody-mobile-foody-bun-cha-ca-ngu-335-635896777195091193.jpg",
          ],
          current_time: 491.0883333333333,
          moving_time: 11.088333333333333,
          distance: 5.4445,
        },
        {
          _id: "5e81a001deeff0201812715d",
          name: "Trung tâm Hành chính Đà Nẵng",
          verify: false,
          category: {
            _id: "5e65c9b3f2527209d2d66b01",
            name: "Special",
            sub_categories: [
              {
                _id: "5e8163e20f0e550cf494062a",
                name: "Morning",
              },
              {
                _id: "5e818af9efa4750858d5ccb1",
                name: "Selfie",
              },
              {
                _id: "5e7c6b4b61a838416c24e581",
                name: "Architect",
              },
            ],
          },
          author: {
            _id: "5e81918ddeeff0201812714d",
          },
          description: "24 Đường Trần Phú",
          price: 0,
          latitude: 16.076718,
          longtitude: 108.223041,
          time_duration: 45,
          cover_image:
            "https://znews-photo.zadn.vn/w660/Uploaded/jugtzb/2016_08_12/DSC_9362.jpg",
          images: [
            "https://media.metrip.vn/tour/content/hoenhoen0110_28763774_562550747463714_5834111612985802752_n.jpg",
          ],
          current_time: 540.0566666666667,
          moving_time: 3.9683333333333333,
          distance: 0.8361000000000001,
        },

        {
          _id: "5e81763d0eaccf0007bee1b5",
          name: "Quang Noodle Ba Mua",
          verify: false,
          category: {
            _id: "5e6894a73e20aa00087fe8a6",
            name: "Food",
            sub_categories: [
              {
                _id: "5e8163a10f0e550cf4940628",
                name: "Lunch",
              },
              {
                _id: "5e8163a40f0e550cf4940629",
                name: "Dinner",
              },
            ],
          },
          author: {
            _id: "5e78225265c6b50007beb05b",
          },
          description: "...",
          price: 100000,
          latitude: 16.066474,
          longtitude: 108.243369,
          time_duration: 45,
          cover_image:
            "https://myquangbamua.com.vn/sites/default/files/40658721_1849858318432344_4117718612783923200_n.jpg",
          images: [
            "https://danang.dulichvietnam.com.vn/images/bien-my-khe-da-nang-6.jpg",
          ],
          current_time: 603.2800000000001,
          moving_time: 6.493333333333334,
          distance: 3.1239,
        },
        {
          _id: "5e81a7c1deeff02018127161",
          name: "Bảo tàng Chăm Đà Nẵng",
          verify: false,
          category: {
            _id: "5e65c9b3f2527209d2d66b01",
            name: "Special",
            sub_categories: [
              {
                _id: "5e8163e70f0e550cf494062b",
                name: "Afternoon",
              },
              {
                _id: "5e7c6b3a61a838416c24e57d",
                name: "Crowded",
              },
              {
                _id: "5e7c6b4761a838416c24e580",
                name: "Historical",
              },
            ],
          },
          author: {
            _id: "5e81918ddeeff0201812714d",
          },
          description:
            "Số 02 2 Tháng 9, Bình Hiên, Hải Châu, Đà Nẵng 550000, Việt Nam",
          price: 60000,
          latitude: 16.060491,
          longtitude: 108.223274,
          time_duration: 60,
          cover_image:
            "https://mytourcdn.com/upload_images/Image/Location/13_3_2015/bao-tang-cham-pa-da-nang-mytour-1.jpg",
          images: [
            "https://image.thanhnien.vn/768/uploaded/congthang/2019_04_02/5b4747795danh_dk_1_dgra.jpg",
          ],
          current_time: 654.1650000000001,
          moving_time: 5.885000000000001,
          distance: 2.5974,
        },

        {
          _id: "5e81a7a75be3ec0007ec0574",
          name: "Bảo Tàng Đà Nẵng",
          verify: false,
          category: {
            _id: "5e65c9b3f2527209d2d66b01",
            name: "Special",
            sub_categories: [
              {
                _id: "5e7c6b4761a838416c24e580",
                name: "Historical",
              },
              {
                _id: "5e7c6b4b61a838416c24e581",
                name: "Architect",
              },
              {
                _id: "5e7c6b3f61a838416c24e57e",
                name: "Culture",
              },
              {
                _id: "5e8163eb0f0e550cf494062c",
                name: "Night",
              },
              {
                _id: "5e8163e70f0e550cf494062b",
                name: "Afternoon",
              },
            ],
          },
          author: {
            _id: "5e78225265c6b50007beb05b",
          },
          description: "...",
          price: 100000,
          latitude: 16.076691,
          longtitude: 108.221631,
          time_duration: 90,
          cover_image:
            "https://t1.daumcdn.net/cfile/tistory/99B751445A82627B02",
          images: [
            "https://danangsensetravel.com/view-800/at_da-nang-co-bao-nhieu-vien-bao-tang-de-toi-kham-pha-2018_7556769474198d9a9fb959a9db9b20cd.jpeg",
          ],
          current_time: 1228.8933333333332,
          moving_time: 9.488333333333333,
          distance: 3.767,
        },
        {
          _id: "5e6891ea3e20aa00087fe8a2",
          name: "Cung Thiếu nhi Đà Nẵng",
          verify: false,
          category: {
            _id: "5e65c9b3f2527209d2d66b01",
            name: "Special",
            sub_categories: [
              {
                _id: "5e8163e70f0e550cf494062b",
                name: "Afternoon",
              },
            ],
          },
          author: {
            _id: "5e688ffa3e20aa00087fe89f",
          },
          description: "....",
          price: 195000,
          latitude: 16.03891,
          longtitude: 108.225414,
          time_duration: 60,
          cover_image:
            "https://vntrip.cdn.vccloud.vn/cam-nang/wp-content/uploads/2018/05/khungcanh-nhavanhoathieunhi-vntrip-e1525848055574.jpg",
          images: [
            "https://vntrip.cdn.vccloud.vn/cam-nang/wp-content/uploads/2018/05/cungvanhoathieunhi-danang-vntrip.jpg",
            "https://kenh14cdn.com/thumb_w/640/2016/1-14-1472742696057-230-0-3648-5468-crop-1472745984780.jpg",
          ],
          current_time: 926.225,
          moving_time: 31.66166666666667,
          distance: 5.518800000000001,
        },
        {
          _id: "5e8196aeefa4750858d5ccb8",
          name: "Ngôi Nhà Đảo Ngược",
          verify: false,
          category: {
            _id: "5e65c9b3f2527209d2d66b01",
            name: "Special",
            sub_categories: [
              {
                _id: "5e818af9efa4750858d5ccb1",
                name: "Selfie",
              },
              {
                _id: "5e8163e20f0e550cf494062a",
                name: "Morning",
              },
              {
                _id: "5e8163e70f0e550cf494062b",
                name: "Afternoon",
              },
              {
                _id: "5e7c6b2e61a838416c24e57b",
                name: "Family",
              },
            ],
          },
          author: {
            _id: "5e5fb95a0e1c8f29003f3157",
          },
          description:
            "Chẳng cần phải lặn lội xa xôi đến check in ở Up Coffee Sài Gòn hay Upside down house Vũng Tàu, giờ đây, các bạn đã có thể thử cảm giác “ngược lên ngược xuống” cùng với ngôi nhà đảo ngược Đà Nẵng. Bất chấp mọi quy luật vật lý, Upside down world Đà Nẵng đang là địa điểm check in được giới trẻ rần rần săn đón.",
          price: 130000,
          latitude: 16.038884,
          longtitude: 108.2448566,
          time_duration: 60,
          cover_image:
            "https://dulichkhampha24.com/wp-content/uploads/2019/12/gia-ve-ngoi-nha-up-nguoc-2.jpg",
          images: [
            "https://danangfantasticity.com/wp-content/uploads/2018/01/nha-dao-nguoc-upside-world-da-nang-04.jpeg",
            "https://hanamihotel.com/wp-content/uploads/2019/11/nh%C3%A0-%C4%91%E1%BA%A3o-ng%C6%B0%E1%BB%A3c-up-side-down-world-%C4%91%C3%A0-n%E1%BA%B5ng.jpg",
            "https://danang-shopping.com/wp-content/uploads/2018/01/nha-dao-nguoc-upside-world-da-nang-1-800x529.jpeg",
          ],
          current_time: 1011.1566666666666,
          moving_time: 24.93166666666667,
          distance: 4.0332,
        },
        {
          _id: "5e81a63d5be3ec0007ec0572",
          name: "Draff Beer",
          verify: false,
          category: {
            _id: "5e6894a73e20aa00087fe8a6",
            name: "Food",
            sub_categories: [
              {
                _id: "5e7c6d9e61a838416c24e58e",
                name: "Beer",
              },
              {
                _id: "5e7c6e0261a838416c24e591",
                name: "Vietnamese",
              },
              {
                _id: "5e7c6e6961a838416c24e594",
                name: "Restaurant",
              },
              {
                _id: "5e8163a40f0e550cf4940629",
                name: "Dinner",
              },
            ],
          },
          author: {
            _id: "5e78225265c6b50007beb05b",
          },
          description:
            "Trần Hưng Đạo, An Hải Tây, Sơn Trà, Đà Nẵng 550000, Việt Nam",
          price: 100000,
          latitude: 16.058622,
          longtitude: 108.230312,
          time_duration: 90,
          cover_image:
            "https://res.cloudinary.com/goeat-me/image/upload/dpr_auto/v1/gem.media/t/7514723507672911872/3c1245f1-6508-4946-8e2c-f4230d332609.jpg",
          images: [
            "https://danang.dulichvietnam.com.vn/images/bien-my-khe-da-nang-6.jpg",
          ],
          current_time: 1019.0916666666666,
          moving_time: 7.9350000000000005,
          distance: 3.5418000000000003,
        },
        {
          _id: "5e68547dfb347f0894bcbc66",
          name: "Cầu Rồng",
          verify: false,
          category: {
            _id: "5e65c9b3f2527209d2d66b01",
            name: "Special",
            sub_categories: [
              {
                _id: "5e675d5141076c1620ce8d05",
                name: "Bridge",
              },
              {
                _id: "5e8163eb0f0e550cf494062c",
                name: "Night",
              },
            ],
          },
          author: {
            _id: "5e5fb95a0e1c8f29003f3157",
          },
          description: "An Hải, Hải Châu, Đà Nẵng 550000, Việt Nam",
          price: 200000,
          latitude: 16.061054,
          longtitude: 108.226531,
          time_duration: 60,
          cover_image:
            "https://benduthuyendanang.com/wp-content/uploads/Cau-Rong-Da-Nang-3.jpg",
          images: [
            "https://i.insider.com/55ba87b8dd0895c81c8b4581?width=1100&format=jpeg&auto=webp",
            "https://i.insider.com/55ba87b8dd0895c81c8b4581?width=1100&format=jpeg&auto=webp",
          ],
          current_time: 1159.405,
          moving_time: 4.2,
          distance: 1.6260999999999999,
        },
      ],
    },
    {
      time_start: 480,
      time_end: 1281.6666666666667,
      length: 15,
      spots: [
        {
          _id: "5e8c36115b756d21589168c1",
          name: "Adamo Hotel Đà Nẵng",
          verify: false,
          address: "304 Võ Nguyên Giáp, P. Mỹ An, Ngũ Hành Sơn, Đà Nẵng.",
          city: "Da Nang",
          district: "Ngu Hanh Son",
          street: "Vo Nguyen Giap",
          category: {
            _id: "5e6608d18f8c030008343d16",
            name: "Stay",
            sub_categories: [
              {
                _id: "5e6608e68f8c030008343d17",
                name: "Hotel",
              },
              {
                _id: "5e6608f38f8c030008343d18",
                name: "Luxury",
              },
            ],
          },
          author: {
            _id: "5e8975d6d86bde1dd05054f8",
          },
          description: "Luxury Adamo Hotel ",
          price: 1330000,
          latitude: 16.048951,
          longtitude: 108.248954,
          time_duration: 0,
          cover_image:
            "https://r-cf.bstatic.com/images/hotel/max1024x768/105/105272540.jpg",
          images: [
            "https://r-cf.bstatic.com/images/hotel/max1280x900/115/115979759.jpg",
          ],
          current_time: 480,
          moving_time: 0,
          distance: 0,
        },
        {
          _id: "5e818ee2deeff0201812714c",
          name: "Công Viên 29 Tháng 3",
          verify: false,
          category: {
            _id: "5e65c9b3f2527209d2d66b01",
            name: "Special",
            sub_categories: [
              {
                _id: "5e8163e20f0e550cf494062a",
                name: "Morning",
              },
              {
                _id: "5e818af9efa4750858d5ccb1",
                name: "Selfie",
              },
            ],
          },
          author: {
            _id: "5e659a946ced7f03ccad258b",
          },
          description:
            "33 Nguyễn Tri Phương, Thạc Gián, Thanh Khê, Đà Nẵng 550000, Việt Nam",
          price: 0,
          latitude: 16.062589,
          longtitude: 108.204586,
          time_duration: 120,
          cover_image:
            "https://baodanang.vn/dataimages/201811/original/images1484307_DSC_9894.JPG",
          images: [
            "https://lh3.googleusercontent.com/proxy/hgXNoqSu5lZEYVRImfrlq4GKBzQ0-to4lH4k3QaC3VZuKKb4qt7Vi27lmr7lhk8IMiNMUz7ZxKW5w2zK42zj_Yn3cVUoS0RI__6BOfh_9uQDBk7S4IxdDMcigrzCfTAMYDOFvtR1ULoodGNaxv_uyuEyTrwazG4VwZ54WThx1CyMeC1hI39C_aI",
          ],
          current_time: 597.615,
          moving_time: 11.729999999999999,
          distance: 3.5012,
        },
        {
          _id: "5e6af1be5ecfe01c1cd41b71",
          name: "Thai market",
          verify: false,
          category: {
            _id: "5e6894a73e20aa00087fe8a6",
            name: "Food",
            sub_categories: [
              {
                _id: "5e6aeeee5ecfe01c1cd41b6d",
                name: "Thai",
              },
              {
                _id: "5e6a0283eeec8116c8ce3f22",
                name: "Spicy",
              },
              {
                _id: "5e8163a40f0e550cf4940629",
                name: "Dinner",
              },
            ],
          },
          author: {
            _id: "5e5fb95a0e1c8f29003f3157",
          },
          description:
            "Nhà hàng món Thái tại Đà Nẵng , Việt Nam - Chuyên món Thái, Ẩm thực Thái",
          price: 200000,
          latitude: 16.06617,
          longtitude: 108.224246,
          time_duration: 60,
          cover_image:
            "https://danangz.vn/wp-content/uploads/2018/09/tuyen-dung-da-nang-6.jpg",
          images: [
            "https://images.foody.vn/video/s800x450/foody-upload-api-foody-gongcha2-636614813106112663-180509164732.jpg",
          ],
          current_time: 607.5883333333334,
          moving_time: 9.973333333333333,
          distance: 4.686100000000001,
        },
        {
          _id: "5e8193ccdeeff02018127154",
          name: "Nhà Thờ Con Gà",
          verify: false,
          category: {
            _id: "5e65c9b3f2527209d2d66b01",
            name: "Special",
            sub_categories: [
              {
                _id: "5e8163e70f0e550cf494062b",
                name: "Afternoon",
              },
              {
                _id: "5e818af9efa4750858d5ccb1",
                name: "Selfie",
              },
              {
                _id: "5e7c6b4b61a838416c24e581",
                name: "Architect",
              },
            ],
          },
          author: {
            _id: "5e81918ddeeff0201812714d",
          },
          description:
            "156 Đường Trần Phú, Hải Châu 1, Hải Châu, Đà Nẵng 550000, Việt Nam",
          price: 0,
          latitude: 16.06668,
          longtitude: 108.223261,
          time_duration: 30,
          cover_image:
            "https://www.taidanang.com/wp-content/uploads/2018/11/nha-tho.jpg",
          images: [
            "https://cdn.vietnammoi.vn/2019/10/8/3850052221862232216602953538551653126897664n-1570534884621955083237.jpg",
          ],
          current_time: 669.8283333333334,
          moving_time: 2.24,
          distance: 0.3872,
        },

        {
          _id: "5e6893f83e20aa00087fe8a4",
          name: "Công Viên APEC",
          verify: false,
          category: {
            _id: "5e65c9b3f2527209d2d66b01",
            name: "Special",
            sub_categories: [
              {
                _id: "5e8163e70f0e550cf494062b",
                name: "Afternoon",
              },
            ],
          },
          author: {
            _id: "5e688ffa3e20aa00087fe89f",
          },
          description:
            "Bạn tới Đà Nẵng đừng bỏ qua công viên này. Nơi có thể ngắm cầu Rồng. Bạn sẽ được xem cảnh rồng phun lửa vào buổi tối. Nhớ ghi lại những khung cảnh đẹp nhất và đầy kỷ niệm và cảm xúc nơi đây nhé!",
          price: 0,
          latitude: 16.058817,
          longtitude: 108.223422,
          time_duration: 45,
          cover_image:
            "https://icdn.dantri.com.vn/thumb_w/640/2017/4-1510197968062.jpg",
          images: [
            "https://congthuong.vn/stores/news_dataimages/mt_vule/102018/02/13/in_article/4701_cong_vien_APEC.jpg",
            "https://media.laodong.vn/Storage/NewsPortal/2018/10/1/633743/DSCF5728.jpg",
          ],
          current_time: 765.4566666666667,
          moving_time: 2.795,
          distance: 1.0796,
        },
        {
          _id: "5e6895763e20aa00087fe8a7",
          name: "Bãi tắm Phạm Văn Đồng",
          verify: false,
          category: {
            _id: "5e65c9b3f2527209d2d66b01",
            name: "Special",
            sub_categories: [
              {
                _id: "5e6894993e20aa00087fe8a5",
                name: "Beach",
              },
              {
                _id: "5e8163e70f0e550cf494062b",
                name: "Afternoon",
              },
            ],
          },
          author: {
            _id: "5e688ffa3e20aa00087fe89f",
          },
          description: "....!",
          price: 0,
          latitude: 16.073521,
          longtitude: 108.246869,
          time_duration: 60,
          cover_image:
            "https://valenciahotel.com.vn/wp-content/uploads/2014/09/bai-tam-pham-van-dong.jpg",
          images: [
            "https://live.staticflickr.com/5543/14334826762_1e23f3f5a0_b.jpg",
            "https://lh3.googleusercontent.com/proxy/WB7LqEwo2lgqxQVSP8Mhg8faTyZHqNIPMsbT-emZoCQ-BMqcvBHjI5TcQVEipToioFLxbUN8slmsefAhJ2Q-KM5vVQV8adupR1aEw1_qmWQO0lCNwB6Gfw5cqwte-IWP_es4Dm4JRYw2Pn0",
          ],
          current_time: 919.0066666666667,
          moving_time: 11.531666666666666,
          distance: 4.4707,
        },
        {
          _id: "5e818cbcdeeff02018127149",
          name: "Bảo tàng 3D TrickEye",
          verify: false,
          category: {
            _id: "5e65c9b3f2527209d2d66b01",
            name: "Special",
            sub_categories: [
              {
                _id: "5e8163e70f0e550cf494062b",
                name: "Afternoon",
              },
              {
                _id: "5e7c6b4b61a838416c24e581",
                name: "Architect",
              },
            ],
          },
          author: {
            _id: "5e659a946ced7f03ccad258b",
          },
          description: "Trần Nhân Tông, Thọ Quang, Sơn Trà, Đà Nẵng, Việt Nam",
          price: 120000,
          latitude: 16.095497,
          longtitude: 108.242954,
          time_duration: 60,
          cover_image:
            "https://danang-shopping.com/wp-content/uploads/2017/07/bao-tang-tranh-3d-tai-da-nang-art-in-paradise-1.jpg",
          images: [
            "https://tourismdanang.com/wp-content/uploads/2017/07/art-in-paradise-18.jpg",
          ],
          current_time: 986.84,
          moving_time: 7.833333333333333,
          distance: 3.2239,
        },
        {
          _id: "5e81a5535be3ec0007ec0571",
          name: "Mì Quảng Ếch Bếp Trang",
          verify: false,
          category: {
            _id: "5e6894a73e20aa00087fe8a6",
            name: "Food",
            sub_categories: [
              {
                _id: "5e8163a10f0e550cf4940628",
                name: "Lunch",
              },
              {
                _id: "5e8163a40f0e550cf4940629",
                name: "Dinner",
              },
              {
                _id: "5e81639d0f0e550cf4940627",
                name: "Breakfast",
              },
              {
                _id: "5e7c6e6961a838416c24e594",
                name: "Restaurant",
              },
            ],
          },
          author: {
            _id: "5e78225265c6b50007beb05b",
          },
          description: "...",
          price: 100000,
          latitude: 16.068227,
          longtitude: 108.244335,
          time_duration: 45,
          cover_image:
            "https://d3av3o1z276gfa.cloudfront.net/images/place/F7GTh8Q0PGCdZPdnbGlwifuxW02mEld7.jpeg",
          images: [
            "https://danang.dulichvietnam.com.vn/images/bien-my-khe-da-nang-6.jpg",
          ],
          current_time: 993.0883333333334,
          moving_time: 6.248333333333333,
          distance: 2.4658,
        },
        {
          _id: "5e8175250eaccf0007bee1b3",
          name: "Cầu Tình yêu",
          verify: false,
          category: {
            _id: "5e65c9b3f2527209d2d66b01",
            name: "Special",
            sub_categories: [
              {
                _id: "5e8163eb0f0e550cf494062c",
                name: "Night",
              },
            ],
          },
          author: {
            _id: "5e78225265c6b50007beb05b",
          },
          description: "...",
          price: 100000,
          latitude: 16.063023,
          longtitude: 108.229849,
          time_duration: 45,
          cover_image:
            "https://www.taidanang.com/wp-content/uploads/2018/09/cau-khoa-tinh-yeu-da-nang-1.jpg",
          images: [
            "https://dulichdaiduong.vn/upload/dia-danh-dep-tai-da-nang.jpg",
          ],
          current_time: 1044.55,
          moving_time: 6.461666666666667,
          distance: 2.5498000000000003,
        },
        {
          _id: "5e8173720eaccf0007bee1af",
          name: "Bar Sky 36",
          verify: false,
          category: {
            _id: "5e65c9b3f2527209d2d66b01",
            name: "Special",
            sub_categories: [
              {
                _id: "5e8163eb0f0e550cf494062c",
                name: "Night",
              },
            ],
          },
          author: {
            _id: "5e78225265c6b50007beb05b",
          },
          description: "Bar View đẹp nhất Đà Nẵng",
          price: 100000,
          latitude: 16.077596,
          longtitude: 108.223541,
          time_duration: 120,
          cover_image:
            "https://static.asiawebdirect.com/m/bangkok/portals/vietnam/homepage/da-nang/nightlife/sky36-club/pagePropertiesImage/sky36-cub-novotel-danang-01.jpg",
          images: [
            "https://media-cdn.tripadvisor.com/media/photo-s/06/a9/bf/c5/sky-36.jpg",
          ],
          current_time: 1161.6666666666667,
          moving_time: 7.916666666666667,
          distance: 3.6927,
        },
      ],
    },
  ],
};

const spot = {
  spots: _.mapKeys(
    Object.assign(
      {},
      initialData.schedules.reduce((prev, next) => [
        ...prev.spots,
        ...next.spots,
      ])
    ),
    (key, value) => {
      return key._id;
    }
  ),
};

const listSpotIds = {listSpotIds: Object.keys(spot.spots)}

// console.log(spot)
/**
 *  {
 *  "0":{
 *    id: "0",
 *    spotIds: ["1232","212312","3123132434","523424564"]
 *  },
 *  "1":{
 *    id: "1",
 *    spotIds: ["756456456","664564646423"]
 *  }
 * }
 */

const column = {
  columns: Object.assign(
    {},
    initialData.schedules.reduce((acc, cur, index) => {
      return [
        ...acc,
        {
          id: index.toString(),
          time_start: cur.time_start,
          time_end: cur.time_end,
          spotIds: cur.spots.map((item) => item._id),
        },
      ];
    }, [])
  ),
};

const columnOrder = { columnOrder: Object.keys(initialData.schedules) };
// console.log(columnOrder)

const newInitialData = { ...initialData, ...spot, ...column, ...columnOrder, ...listSpotIds };
// console.log(newInitialData.list.map(i => i));
export default newInitialData;
