import React, { Component } from "react";
import styled from "styled-components";
import { DragDropContext, Droppable } from "react-beautiful-dnd";
import { Icon, Button, Dimmer, Loader } from "semantic-ui-react";
import _ from "lodash";
import axios from "axios";
import uuid from "react-uuid";
import { getToken, getUser } from "../../utils/Common";
import DatePicker from "react-datepicker";
import { default as NumberFormat } from "react-number-format";
import { generateDateArray } from "../../utils/DateTimeHandler";
import UploadImage from '../../components/UploadImage/UploadImage'
import Column from "./Column";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import serverURL from '../../utils/Common';

const Container = styled.div`
  display: flex;
  margin: 2rem 12%;
  background: white;
  overflow-x: scroll;
  padding: 1rem;
`;

const Content = styled.div``;
const Right = styled.div`
  display: flex;
  align-items: center;
`;
const Left = styled.div`
  text-align: start;
`;
const Image = styled.img`
  width: 24px;
`;
const Header = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 1rem 12%;
  background: white;
  box-shadow: rgba(0, 0, 0, 0.3) 0px 0px 6px 0px;
`;
const Title = styled.input`
  font-weight: 600;
  width:92%;
  font-size: 20px;
  font-weight: 600;
  text-align: start;
  text-transform: capitalize;
  border: 0;
  border-bottom: 1px solid rgba(0, 0, 0, 0.05);
  padding: 0.5rem 0;
  outline: none;
  ::placeholder {
    color: #d4d4d4;
  }
`;

const SubTitle = styled.div`
  display: flex;
  text-align: start;
  margin: 1rem 0;
`;
const Load = styled.div`
  height: 10rem;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const DateForm = styled.div`
  width: 100%;
`;
const DateGroup = styled.div`
  display: flex;
  align-items: center;
  margin: 1rem 0;
  & > .react-datepicker-wrapper {
    margin-left: auto;
    .react-datepicker__input-container {
      input {
        outline: none;
        border: 0;
        border-bottom: 1px solid rgba(0, 0, 0, 0.05);
        padding: 0.2rem 0.5rem;
      }
    }
  }
`;
const ItemGroup = styled.div`
  display: flex;
  align-items: center;
  &:nth-child(2){
    margin-left:15%;
  }

`;
const Input = styled.span`
  margin-left: 1rem;
`;
const InputImage = styled.input`
  border: 0;
  border-bottom: 1px solid rgba(0, 0, 0, 0.05);
  padding: 0.5rem 0;
  outline: none;
  width: 63%;
  margin-left: auto;
`;
export default class EditTrip extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isChangeName: false
    };
  }

  componentDidMount() {
    let _id = window.location.pathname.slice(6, 30);
    console.log(window.location.pathname.slice(1, 6));
    console.log("test path: ", window.location.pathname);
    if (
      window.location.pathname.slice(1, 6) === "trip/" &&
      window.location.pathname != "/trip/add"
    ) {
      axios.get(`${serverURL}/trip/${_id}`).then((res) => {
        let initialData;
        initialData = res.data.data;
        let result = [];
        for (let item of initialData.schedules) {
          for (let test of item.spots) {
            result.push(test);
          }
        }

        let spot = {
          spots: _.mapKeys(Object.assign({}, result), (key, value) => {
            return key._id;
          }),
        };

        const listSpotIds = { listSpotIds: Object.keys(spot.spots) };

        const column = {
          columns: Object.assign(
            {},
            initialData.schedules.reduce((acc, cur, index) => {
              return [
                ...acc,
                {
                  id: index.toString(),
                  spotIds: cur.spots.map((item) => item._id),
                },
              ];
            }, [])
          ),
        };
        const columnOrder = { columnOrder: Object.keys(initialData.schedules) };
        let now = moment();
        let date_start = now.format("YYYY-MM-DD");
        date_start = Date.parse(date_start);
        let date_end = now.add(parseInt(initialData.number_of_day)-1, 'days').format("YYYY-MM-DD");
        date_end = Date.parse(date_end);

        const newInitialData = {
          // text: "Rename",
          ...initialData, 
          date_start: date_start,
          date_end: date_end,
          ...spot,
          ...column,
          ...columnOrder,
          ...listSpotIds,
        };

        //this.setState({ newInitialData });
        this.setState(newInitialData);
      });
    }
    /**
     * new trip create by user
     */
    if (window.location.pathname === "/trip/add") {
      let generate = JSON.parse(localStorage.getItem("generate"));
      console.log("generate", generate);
      let result = [];
      for (let item of generate.schedules) {
        for (let test of item.spots) {
          result.push(test);
        }
      }

      let spot = {
        spots: _.mapKeys(Object.assign({}, result), (key, value) => {
          return key._id;
        }),
      };

      const listSpotIds = { listSpotIds: Object.keys(spot.spots) };

      const column = {
        columns: Object.assign(
          {},
          generate.schedules.reduce((acc, cur, index) => {
            return [
              ...acc,
              {
                id: index.toString(),
                spotIds: cur.spots.map((item) => item._id),
              },
            ];
          }, [])
        ),
      };
      const columnOrder = { columnOrder: Object.keys(generate.schedules) };
      const newgenerate = {
        // text: "Rename",
        ...generate,
        ...spot,
        ...column,
        ...columnOrder,
        ...listSpotIds,
      };

      this.setState(newgenerate);
    }
    let btnUpdate = document.getElementById("btn-update");
    let btnSave = document.getElementById("btn-save");

    if (this.state._id) {
      btnSave.disabled = false;
      btnUpdate.disabled = true;
    }
  }
  componentDidUpdate(prevProp, prevState) {
    let btnUpdate = document.getElementById("btn-update");
    let content = document.getElementById("content");
    let loadUpdate = document.getElementById("load-update");
    let btnSave = document.getElementById("btn-save");

    let generate = JSON.parse(localStorage.getItem("generate"));
    if (this.state._id || generate.schedules) {
      if (prevState._id || prevState.schedules) {
        if (this.state.schedules !== prevState.schedules) {
          btnUpdate.disabled = false;
          btnSave.disabled = true;
        }
      } else {
        // btnSave.disabled = false;
        btnUpdate.disabled = true;
      }

      content.style.display = "block";
      loadUpdate.style.display = "none";
    }
  }

  onDragEnd = (result) => {
    let btnUpdate = document.getElementById("btn-update");
    let btnSave = document.getElementById("btn-save");
    btnSave.disabled = true;
    btnUpdate.disabled = false;
    const { destination, source, draggableId, type } = result;

    if (!destination) {
      return;
    }
    if (
      destination.droppableId === source.droppableId &&
      destination.index === source.index
    ) {
      return;
    }

    if (type === "column") {
      const newColumnOrder = Array.from(this.state.columnOrder);
      newColumnOrder.splice(source.index, 1);
      newColumnOrder.splice(destination.index, 0, draggableId);

      const newStates = {
        ...this.state,
        columnOrder: newColumnOrder,
      };
      console.log(newStates);
      this.setState(newStates);
      return;
    }

    const start = this.state.columns[source.droppableId];
    const finish = this.state.columns[destination.droppableId];

    // console.log(start);
    if (start === finish) {

      const newSpotIds = Array.from(start.spotIds);

      newSpotIds.splice(source.index, 1);
      newSpotIds.splice(destination.index, 0, draggableId);

      const newColumn = {
        ...start,
        spotIds: newSpotIds,
      };
      this.setState({
        ...this.state,
        columns: {
          ...this.state.columns,
          [newColumn.id]: newColumn,
        },
      });
      return;
    }

    //moving from one list to another
    const startTaskIds = Array.from(start.spotIds);
    startTaskIds.splice(source.index, 1);
    const newStart = {
      ...start,
      spotIds: startTaskIds,
    };

    const finishTaskIds = Array.from(finish.spotIds);

    finishTaskIds.splice(destination.index, 0, draggableId);

    const newfinsh = {
      ...finish,
      spotIds: finishTaskIds,
    };

    this.setState({
      ...this.state,
      columns: {
        ...this.state.columns,
        [newStart.id]: newStart,
        [newfinsh.id]: newfinsh,
      },
    });
    
  };

  /**
   *  Remove spot
   */
  removeSpot = (item) => {
    let btnUpdate = document.getElementById("btn-update");
    btnUpdate.disabled = false;
    const columnId = this.state.columns[item.columnId];
    if (columnId.spotIds.length > 2) {
      const newSpotIds = columnId.spotIds.filter(
        (spotId) => spotId !== item.spotId
      );
      const newListSpotIds = this.state.listSpotIds.filter(
        (spotId) => spotId !== item.spotId
      );

      const newColumn = {
        ...columnId,
        spotIds: newSpotIds,
      };

      this.setState({
        ...this.state,
        listSpotIds: newListSpotIds,
        columns: {
          ...this.state.columns,
          [newColumn.id]: newColumn,
        },
      });
    } else {
      alert("Minium 2 spots");
    }
  };
  /**
   *  Add spot nearby
   */
  addSpot = (item) => {
    let btnUpdate = document.getElementById("btn-update");
    let btnSave = document.getElementById("btn-save");
    btnSave.disabled = true;
    btnUpdate.disabled = false;
    const columnId = this.state.columns[item.columnId];
    const newSpotIds = Array.from(columnId.spotIds);
    newSpotIds.splice(item.spotIndex + 1, 0, item.newSpot._id); // add id after index choose nearby
    const spot = item.newSpot;

    /**
     * Update item in column
     */
    const newColumn = {
      ...columnId,
      spotIds: newSpotIds,
    };
    const newState = {
      ...this.state,
      spots: {
        ...this.state.spots,
        [spot._id]: spot,
      },
      columns: {
        ...this.state.columns,
        [newColumn.id]: newColumn,
      },
    };
    this.setState(newState);
  };
  isValidation(){
    if(this.state.name ==""){
      alert('Name is required!');
      return false;
    } 
    else if(!this.state.isChangeName){
      alert(`Change your trip's name`);
      return false;
    }
    return true;
  }
  isChangeName(newName){
    if(this.state.name == newName){
      alert(`Change your trip's name`);
      return false;
    }
    return true;
  }

  saveTrip = () => {
    
    if(this.isValidation()){
      var config = {
        headers: {
          "auth-token": getToken(),
        },
      };
      console.log('this.state.name: ', this.state.name);
      let btnSave = document.getElementById("btn-save");
      //btnSave.disabled = true;
  
      let newData = {
        list: this.state.columnOrder.map((columnId) => {
          const column = this.state.columns[columnId];
          const spot = column.spotIds.map((spotId) => this.state.spots[spotId]);
          return spot;
        }),
      };
      const COVER_IMAGE = "https://e-travel-s3-bucket.s3.ap-southeast-1.amazonaws.com/image/bd0c94fbabd9ba334a45d6fccf4f9392";
      const newState = {
        _id: this.state._id ? this.state._id : null,
        name: (this.state.name) ? this.state.name : uuid(),
        sub_categories: this.state.sub_categories,
        date_start: this.state.date_start,
        date_end: this.state.date_end,
        number_of_day: this.state.number_of_day,
        kid: this.state.kid,
        adult: this.state.adult,
        budget: this.state.budget,
        rating: this.state.rating,
        cover_image: this.state.cover_image? this.state.cover_image: COVER_IMAGE,
        creator: this.state.creator,
        schedules: [
          ...newData.list.map((spots, index) => {
            return {
              ...this.state.schedules[index],
              spots,
            };
          }),
        ],
      };
      console.log("saveTrip -> newState", newState)
  
      //Call API save trip by axios
      //send request to server and assign newState for axios
  
      axios
        .post(`${window.$serverURL}/trip`, newState, config)
        .then((res) => {
          
          localStorage.removeItem("generate");
          window.location.href = "/info/trips";
        })
        .catch((error) => {
          if (error.response.data.message){
            alert(error.response.data.message);
            console.log(error.response.data);
          } 
        });
    }
    
  };

  updateTrip = () => {
    var config = {
      headers: {
        "auth-token": getToken(),
      },
    };
    /**
     * handle button save, update
     */
    let btnSave = document.getElementById("btn-save");
    let content = document.getElementById("content");
    let loadUpdate = document.getElementById("load-update");
    btnSave.disabled = false;
    content.style.display = "none";
    loadUpdate.style.display = "block";

    let newData = {
      list: this.state.columnOrder.map((columnId) => {
        const column = this.state.columns[columnId];
        const spot = column.spotIds.map((spotId) => this.state.spots[spotId]);
        return spot;
      }),
    };

    let newState = {
      schedules: [
        ...newData.list.map((spots, index) => {
          return {
            ...this.state.schedules[index],
            spots,
          };
        }),
      ],
    };
    axios
      .post(
        `${serverURL}/distance/schedule/update`,
        newState,
        config
      )
      .then((res) => {
        let initialData;
        initialData = res.data.data;
        console.log('will test: ',res.data.data);
        let result = [];
        for (let item of initialData.schedules) {
          for (let test of item.spots) {
            result.push(test);
          }
        }

        let spot = {
          spots: _.mapKeys(Object.assign({}, result), (key, value) => {
            return key._id;
          }),
        };

        const listSpotIds = { listSpotIds: Object.keys(spot.spots) };

        const column = {
          columns: Object.assign(
            {},
            initialData.schedules.reduce((acc, cur, index) => {
              return [
                ...acc,
                {
                  id: index.toString(),
                  spotIds: cur.spots.map((item) => item._id),
                },
              ];
            }, [])
          ),
        };

        const columnOrder = { columnOrder: Object.keys(initialData.schedules) };

        const newInitialData = {
          ...this.state,
          ...spot,
          ...column,
          ...columnOrder,
          ...listSpotIds,
        };
        
        this.setState(newInitialData);
        this.setState({...this.state, budget: res.data.data.budget});
        let btnUpdate = document.getElementById("btn-update");
        let btnSave = document.getElementById("btn-save");
        btnUpdate.disabled = true;
        btnSave.disabled = false;
        console.log(this.state);
      });
  };
  cover_image = (event) => {
    console.log("cover_image -> event", event);

    this.setState({ ...this.state, cover_image: event.target.value });
  };
  onKeyup = (event) => {
    if (event.keyCode === 13) {
      let text = event.target.value;
      if (!text) {
        return;
      }
      text = text.trim();

      if (!text) {
        return;
      }
      this.setState({
        ...this.state,
        name: text,
      });
    }
  };
  onChange = (event) => {
    if(this.isChangeName(event.target.value)){
      this.setState({
        name: event.target.value,
        isChangeName: true
      });
    }
  };
  setStartDate = (date) => {
    console.log("date", date)
    // const date_end = new Date(
    //   `${`${date.getMonth() + 1}  ` +
    //     " " +
    //     `${date.getDate() + this.state.number_of_day - 1}` +
    //     ", " +
    //     date.getFullYear()}`
    // );

    let now = moment(date);
    let date_start = now.format("YYYY-MM-DD");
    date_start = Date.parse(date_start);
    let date_end = now.add(parseInt(this.state.number_of_day -1), 'days').format("YYYY-MM-DD");
    date_end = moment(date_end).toDate();

    console.log("date_end ttt", date_end)
    this.setState({
      ...this.state,
      date_start: date,
      date_end: date_end,
    });
  };
  dataImage = (dt_img) => {
    console.log('dt_img: ',this.state);
    this.setState({
      ...this.state,
      images: dt_img,
      cover_image:dt_img[0]
    });
    console.log("tesst state:", this.state.images, this.state.cover_image)
   
  };
  render() {
    if ((this.state && this.state._id) || this.state.schedules) {
      return (
        <div>
          <Load id="load-update">
            {" "}
            <Dimmer active inverted>
              <Loader inverted>Updating</Loader>
            </Dimmer>
          </Load>
          <Content id="content">
            <Header>
              <Left>
                <Title
                  onKeyUp={this.onKeyup}
                  type="text"
                  placeholder="Trip Name"
                  value={this.state.text}
                  onChange={this.onChange}
                  required
                  defaultValue={this.state.name}
                ></Title>
                <Icon circular name="edit" />
                <SubTitle>
                  <ItemGroup>
                    <strong>People: </strong>{" "}
                    <Input>
                      {parseInt(this.state.kid) + parseInt(this.state.adult)}
                    </Input>
                  </ItemGroup>
                  <ItemGroup>
                    <strong>Budget:</strong>
                    <Input>
                      <NumberFormat
                        value={this.state.budget}
                        displayType={"text"}
                        style={{fontWeight:"400"}}
                        thousandSeparator={true}
                        prefix={" VND "}
                      />
                    </Input>
                  </ItemGroup>
                </SubTitle>
                <DateForm>
                  <DateGroup>
                    <strong>Start Date: </strong>
                    <DatePicker
                      selected={new Date(this.state.date_start)}
                      onChange={(date) => this.setStartDate(date)}
                      selectsStart
                      startDate={this.state.date_start}
                      dateFormat="d/MM/yyyy"
                      minDate={new Date()}
                    />{" "}
                  </DateGroup>
                  <DateGroup>
                    <strong>End Date: </strong>
                    <DatePicker
                      readOnly
                      selected={ new Date(this.state.date_end)}
                      dateFormat="d/MM/yyyy"
                    />
                  </DateGroup>
                </DateForm>
                <ItemGroup>
                  <UploadImage data={this.dataImage}></UploadImage>
                </ItemGroup>
              </Left>
              <Right>
                <Button color="blue" onClick={this.updateTrip} id="btn-update">
                  {" "}
                  Update
                </Button>
                <Button color="blue" onClick={this.saveTrip} id="btn-save">
                  {" "}
                  Save
                </Button>
              </Right>
            </Header>
            <DragDropContext onDragEnd={this.onDragEnd}>
              <Droppable
                droppableId="all-column"
                direction="horizontal"
                type="column"
              >
                {(provided) => (
                  <Container
                    {...provided.droppableProps}
                    ref={provided.innerRef}
                  >
                    {this.state.columnOrder.map((columnId, index) => {
                      const column = this.state.columns[columnId];
                      const spots = column.spotIds.map(
                        (spotId) => this.state.spots[spotId]
                      );
                      const dayArray = generateDateArray(
                        new Date(this.state.date_start),
                        this.state.number_of_day
                      ).map(
                        (day) =>
                          `${
                            day.getDate() < 10
                              ? "0" + day.getDate()
                              : day.getDate()
                          }-${
                            day.getMonth() < 10
                              ? "0" + `${parseInt(day.getMonth()) + 1}`
                              : `${parseInt(day.getMonth()) +1}`
                          }`
                      );
                      const date = dayArray.map((day) => day);
                      console.log(date)
                      return (
                        <Column
                          key={column.id}
                          column={column}
                          spot={spots}
                          onRemove={this.removeSpot}
                          onAdd={this.addSpot}
                          index={index}
                          date_start={this.state.date_start}
                          listSpotIds={this.state.listSpotIds}
                          date={date}
                        />
                      );
                    })}
                    {provided.placeholder}
                  </Container>
                )}
              </Droppable>
            </DragDropContext>
          </Content>
        </div>
      );
    } else {
      return (
        <Load id="load">
          {" "}
          <Dimmer active inverted>
            <Loader inverted>Loading</Loader>
          </Dimmer>
        </Load>
      );
    }
  }
}
