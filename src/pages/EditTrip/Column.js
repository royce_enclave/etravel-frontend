import React from "react";
import styled from "styled-components";
import { Droppable, Draggable } from "react-beautiful-dnd";
import { generateDateArray } from '../../utils/DateTimeHandler'
import _ from 'lodash';
import Spot from "./Spot";

const Column = styled.div`
  background-color: white;
  display: flex;
  flex-direction: column;
`;

const Header = styled.div`
  text-align: start;
  background-color: rgba(245, 245, 245, 0.96);
  padding: 1rem 1.5rem ;
  border: 1px dotted gray;
`;
const Title = styled.h4`
  font-weight: 600;
  margin: 0.5rem 0;
`;
const Number = styled.span``;
const SpotList = styled.div`
  padding: 8px;
  transition: background-color 0.2s ease;
  background-color: ${(props) => (props.isDraggingOver ? "skyblue" : "white")};
  flex-grow: 1;
  min-height: 100px;
  min-width: 356px;
  border: 1px dotted gray;
`;

export default class Columns extends React.Component {
  removeSpot = (spotId) => {
    const columnId = this.props.column.id;
    this.props.onRemove({ spotId, columnId });
  };

  addSpot = (spot) => {
    const columnId = this.props.column.id;
    this.props.onAdd({ ...spot, columnId });
  };

  render() {
    const { column, index, spot, listSpotIds, date, date_start } = this.props;
    const year = new Date(date_start).getFullYear()
    const totalCost = _.sumBy(spot, item => item.price);
    const formatter = new Intl.NumberFormat('ja-JP', {
      minimumFractionDigits: 0
    })
  
    return (
      <Draggable draggableId={column.id} index={index}>
        {(provided) => (
          <Column {...provided.draggableProps} ref={provided.innerRef}>
            <Header {...provided.dragHandleProps}>
              <Title>
                Day {parseInt(column.id, 0) + 1} |{" "}
                {date[parseInt(column.id, 0)] + "-" + year }
              </Title>
              <Number><strong>Spots:</strong> {spot.length}</Number>
              <br/>
              <Number><strong>Total Cost:</strong> ~VND {formatter.format(totalCost) }</Number>
            </Header>
            <Droppable droppableId={column.id} type="task">
              {(provided, snapshot) => (
                <SpotList
                  ref={provided.innerRef}
                  {...provided.droppableProps}
                  isDraggingOver={snapshot.isDraggingOver}
                >
                  {spot.map((spot, index) => (
                    <Spot
                      key={spot._id}
                      spot={spot}
                      index={index}
                      listSpotIds={listSpotIds}
                      removeSpot={this.removeSpot}
                      addSpot={this.addSpot}
                    />
                  ))}
                  {provided.placeholder}
                </SpotList>
              )}
            </Droppable>
          </Column>
        )}
      </Draggable>
    );
  }
}
