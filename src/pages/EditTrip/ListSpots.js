import React from 'react';
import { Dimmer, Loader } from 'semantic-ui-react';
import styled from 'styled-components';

export default function ListSpot(props) {
  /**
   * Style here
   */
  const Container = styled.div`
    margin: 0.5rem 1rem;
  `;
  const Item = styled.div`
    display: flex;
    padding: 1rem 0;
    border-bottom: 1px solid gray;
  `;
  const Content = styled.div`
    display: block;
    margin-left: 1rem;
    width: 100%;
  `;
  const Header = styled.a`
    text-align: start;
    color: black;
    border-bottom: 0;
    font-size: 18px;
    padding: 0;
    &:hover {
      text-decoration: none;
      color: inherit;
    }
  `;
  const Load = styled.div`
    height: 10rem;
    display: flex;
    justify-content: center;
    align-items: center;
  `;
  const Meta = styled.h5`
    text-align: start;
  `;
  const Image = styled.img`
    width: 100%;
    height: 100%;
    background-size: cover;
  `;
  const Extra = styled.div`
    display: flex;
    justify-content: flex-end;
    align-items: flex-end;
  `;
  const Button = styled.button`
    padding: 0.5rem 2rem;
    background: none;
    border: 1px solid gray;
    border-radius: 4px;
    &:hover {
      color: white;
      background-color: #2185d0;
    }
  `;

  const spots = props.spot;
  if (spots.length > 0) {
    return (
      <Container>
        {spots.map((spot, index) => (
          <Item key={index}>
            <div style={{ height: '120px', width: '120px' }}>
              <Image src={spot['cover_image']}></Image>
            </div>

            <Content>
              <Header href={'/spot/' + spot._id + '/detail'} target="_blank">
                {spot.name}
              </Header>
              <Meta>{spot.address}</Meta>
              <p style={{fontSize:"14px", textAlign:"start"}}>{Math.round(spot.distance * 100) / 100} km</p>
              <Extra>
                <Button onClick={() => props.setSpots(spot)}> Add Spot</Button>
              </Extra>
            </Content>
          </Item>
        ))}
      </Container>
    );
  } else {
    return (
      <Load id="load">
        {' '}
        <Dimmer active inverted>
          <Loader inverted>Loading</Loader>
        </Dimmer>
      </Load>
    );
  }
}
