import React, { Component } from "react";
import styled from "styled-components";
import { Tabs, Tab } from "react-bootstrap";
import axios from "axios";
import ListSpot from "./ListSpots";

const Container = styled.div`
  overflow: scroll;
  height: 500px;
`;
export default class ShowPopUp extends Component {
  constructor(props) {
    super(props);
    console.log("popup", props.listSpotIds.map(i => typeof(i)));
    this.state = {
      spots: [],
      listSpotIds: this.props.listSpotIds
    };
  }

  setSpot = (i) => {
    this.props.addSpot(i);
    this.setState({
      spots: this.state.spots.filter(item => item !== i),
      listSpotIds: this.state.listSpotIds.push(i._id)
    })
    this.props.close();
    return
  };

  async componentDidMount() {
    await axios
      .get(
        `${window.$serverURL}/distance/nearby/3000/`+
          `${this.props.spotId}`
      )
      .then((res) => {
        let data = res.data.data;
        if(data && data.length > 0){
         data = data.filter(item => !this.state.listSpotIds.includes(item._id))
          this.setState({ spots: data });
        }
        
      });
    console.log("state", this.state);
  }

  render() {
    return (
      <Container>
        <Tabs defaultActiveKey="home" id="uncontrolled-tab-example">
          <Tab eventKey="home" title="Near By">
            <ListSpot spot={this.state.spots} setSpots={this.setSpot} />
          </Tab>
        </Tabs>
      </Container>
    );
  }
}
