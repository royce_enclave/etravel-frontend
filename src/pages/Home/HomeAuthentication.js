import React, { useState, useEffect } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  useLocation,
} from "react-router-dom";
import axios from "axios";
import Login from "../Login/Login";
import HomePage from "../HomePage/HomePage";
import SignUp from "../Register/Signup";
import PrivateRoute from "../../utils/PrivateRoute";
import PublicRoute from "../../utils/PublicRoute";
import { getToken, setUserSession } from "../../utils/Common";
import "../../../node_modules/bootstrap/dist/css/bootstrap.min.css";
import Header from "../../components/Header/Header";
import MainProfile from "../Profile/MainProfile";
import Footer from "../../components/Footer/Footer";
import SpotsDetail from "../../pages/SpotsDetail/SpotsDetail";
import TripDetail from "../../pages/TripDetail/TripDetail";
import EditTrip from "../../pages/EditTrip/EditTrip";
import CreateTrip from "../../pages/CreateTrip/MainForm";
import AddLocation from "../../pages/AddOnMap/AddLocation";
import Maps from "../../pages/Maps/Maps";
import Admin from "../Admin/Admin";
function HomeAuthentication() {
  const [authLoading, setAuthLoading] = useState(true);

  useEffect(() => {
    const token = getToken();
    if (!token) {
      return;
    }

    axios
      .get
      //`https://o4mande4nk.execute-api.ap-southeast-1.amazonaws.com/verifyToken?token=${token}`
      ()
      .then((response) => {
        console.log(response);
        setUserSession(response.data.token, response.data.user);
        setAuthLoading(false);
      })
      .catch((error) => {
        setAuthLoading(false);
      });
  }, []);

  if (authLoading && getToken()) {
    return <div>Loading...</div>;
  }

  return (
    <Router>
      <Switch>
        <Route path="/admin">
          <Admin />
        </Route>
        <Route path="/">
          <div>
            <div className="App">
              <Header></Header>
              <div
                className="auth-wrapper"
                style={{
                  paddingTop: "74px",
                  backgroundColor: "rgba(245, 245, 245, 0.959)",
                  paddingBottom: "5%",
                }}
              >
                <Switch>
                  <Route exact path="/" component={HomePage} />
                  <PublicRoute path="/login" component={Login} />
                  <PrivateRoute path="/Homepage" component={HomePage} />
                  <PublicRoute path="/register" component={SignUp} />
                  <Route path="/info">
                    <MainProfile />
                  </Route>
                  <Route path="/spot/:id/detail">
                    <SpotsDetail></SpotsDetail>
                  </Route>
                  <Route path="/trip/:id/detail">
                    <TripDetail></TripDetail>
                  </Route>
                  <Route path="/trip/:id/edit">
                    <EditTrip></EditTrip>
                  </Route>
                  <Route path="/trip/add">
                    <CreateTrip></CreateTrip>
                  </Route>
                  <Route path="/spots/add">
                    <AddLocation></AddLocation>
                  </Route>
                  <Route path="/generate/trip">
                    <EditTrip></EditTrip>
                  </Route>
                  <Route path="/map/:id/map">
                    <Maps></Maps>
                  </Route>
                </Switch>
              </div>
              <Footer></Footer>
            </div>
          </div>
        </Route>
      </Switch>
    </Router>
  );
}

export default HomeAuthentication;
