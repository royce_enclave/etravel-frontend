import React, { useState } from "react";
import axios from "axios";
import Avatar from "@material-ui/core/Avatar";
import CssBaseline from "@material-ui/core/CssBaseline";
import Link from "@material-ui/core/Link";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import { makeStyles } from "@material-ui/core/styles";
import styled from "styled-components";
import "./Register.css";
import serverURL from "../../utils/Common"

function SignUp(props) {
  const [loading, setLoading] = useState(false);
  const username = useFormInput("");
  const password = useFormInput("");
  const email = useFormInput("");
  const full_name = useFormInput("");
  const phone_num = useFormInput("");
  const [error,setError] = useState(null);

  // handle button click of Signup form
  const handleSignUp = () => {
    setError(null);
    setLoading(true);
    axios
      .post(`${serverURL}/auth/register`, {
        username: username.value,
        password: password.value,
        email: email.value,
        full_name: full_name.value,
        phone_num: phone_num.value,
      })
      .then((response) => {
        props.history.push("/login");
        alert("Register Sucessfull");
        console.log("OK")
      })
      .catch((error) => {
        alert("Something wrong! Try again.")
      });
  };

  const useStyles = makeStyles((theme) => ({
    image: {
      backgroundImage:
        "url(http://thuthuatphanmem.vn/uploads/2018/05/23/wallpaper-4k-hinh-nen-4k-phong-canh-thien-nhien-dep-4_100018316.jpg)",
      backgroundRepeat: "no-repeat",
      backgroundColor:
        theme.palette.type === "light"
          ? theme.palette.grey[50]
          : theme.palette.grey[900],
      backgroundSize: "cover",
      backgroundPosition: "center",
    },
    paper: {
      margin: theme.spacing(8, 4),
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: "100%",
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));

  const classes = useStyles();
  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={7} className={classes.image} />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <form className={classes.form}>
            <h3>Register</h3>
            <div className="form-group">
             
              <input
                type="text"
                className="form-control"
                placeholder="Username"
                {...username}
              />
            </div>
            <div className="form-group">
              
              <input
                type="password"
                className="form-control"
                placeholder="Password"
                {...password}
              />
            </div>
            <div className="form-group">
              
              <input
                type="text"
                className="form-control"
                placeholder="Full name"
                {...full_name}
              />
            </div>
            <div className="form-group">
          
              <input
                type="text"
                className="form-control"
                placeholder="Email"
                {...email}
              />
            </div>
            <div className="form-group">
             
              <input
                type="text"
                className="form-control"
                placeholder="Phone number"
                {...phone_num}
              />
            </div>
            <button
              type="submit"
              className="button button1"
              loadingtext="Logging in..."
              onClick={handleSignUp}
              disabled={loading}
            >
              Register
            </button>
            <Grid container>
              <Grid item>
                <Link href="/login" variant="body2">
                  {"Did you have an account? Login"}
                </Link>
              </Grid>
            </Grid>
          </form>
        </div>
      </Grid>
    </Grid>
  );
}

const useFormInput = (initialValue) => {
  const [value, setValue] = useState(initialValue);
  const handleChange = (e) => {
    setValue(e.target.value);
  };
  return {
    value,
    onChange: handleChange,
  };
};

export default SignUp;
