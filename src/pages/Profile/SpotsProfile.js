import React, { Component } from 'react';
import axios from 'axios';
import serverURL, { getToken } from '../../utils/Common';
import styled from 'styled-components';
import { BrowserRouter as Router, Link } from 'react-router-dom';
import { MdAddCircle } from 'react-icons/md';

const Header = styled.h4`
  display: flex;
  align-items: center;
  text-align: start;
  text-transform: capitalize;
  height: 4rem;
`;
const Content = styled.div`
  display: flex;
  flex-wrap: wrap;
  background: white;
`;
const Card = styled.div`
  width: 220px;
  height: 300px;
  margin: 0.5rem;
  border: 1px solid gray;
`;
const Title = styled.a`
  font-size: 17px;
  width: 100%;
  text-align: center;
  color: black;
  font-weight: 600;
  padding: 0.5rem !important;
  &:hover {
    text-decoration: none;
    color: inherit;
  }
  border: 0;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;
const Meta = styled.div``;
const Address = styled.div`
  padding: 0.5rem;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

const Container = styled.div`
  margin: 1rem;
`;

const Add = styled.a`
  text-decoration: none;
  padding: 0;
  color: black;

  &:hover {
    text-decoration: none;
    color: blue;
  }
`;
const Top = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
const Center = styled.div`
  margin-top: 3rem;
`;
const Description = styled.div`
  text-transform: capitalize;
`;
const Image = styled.img`
  display: block;
  font-size: 14px;
  height: 200px;
  line-height: 209px;
  text-size-adjust: 100%;
  width: 100%;
`;
const Verified = styled.p`
  color: green;
`
const Unverified = styled.p`
  color: red;
`
class SpotsProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listspots: [],
    };
  }

  componentDidMount() {
    var config = {
      headers: {
        'auth-token': getToken(),
      },
    };
    axios
      .get(`${serverURL}/spot/user`, config)
      .then((response) => {
        this.setState({
          listspots: response.data.data,
        });
        console.log('OK', response.data.data[0].verify);
      })
      .catch((error) => console.log(error));
  }

  render() {
    console.log('profile', this.state);
    if (this.state.listspots && this.state.listspots.length > 0) {
      return (
        <Container>
          <Top>
            <Header>my spots</Header>
            <Add href="/spots/add">Add Spot</Add>
          </Top>
          <Content>
            {this.state.listspots.map((spot) => (
              <Card>
                <Image src={spot['cover_image']} />
                <Meta>
                  <Title  href={(spot.verify)? `/spot/${spot._id}/detail`: ""}>
                    {spot.name}
                  </Title>
                  <Address>{spot.address}</Address>
                  <p>{(spot.verify)?<Verified>Verified</Verified>:<Unverified>Unverified</Unverified>}</p>
                </Meta>
              </Card>
            ))}
          </Content>
        </Container>
      );
    } else {
      return (
        <Center>
          <Title href="/spots/add">
            <MdAddCircle size={64} color="#0000000d" />
            <Description>add your spot</Description>
          </Title>
        </Center>
      );
    }
  }
}

export default SpotsProfile;
