import React, { useState } from "react";
import styled from "styled-components";
import { IoMdPerson } from "react-icons/io";
import { AiOutlineSchedule } from "react-icons/ai";
import { BrowserRouter as NavLink, Link } from "react-router-dom";
import { getUser } from "../../utils/Common";
import Spot from "./spot.svg";

const Item = styled.li`
  display: flex;
  align-items: center;
  text-align: start;
  border-radius: 4px;
  margin: 0.5rem 0;
  padding: 0 1rem;
  text-transform: capitalize;
  & > a {
    text-decoration: none;
    padding: 0.5rem 1rem;
    color: black;
  }
  &:hover {
    background: #efefef;
  }
  &:visited {
    background: red;
  }
`;
const Menu = styled.div`
  margin: 0;
  margin-top: 0.5rem;
  width: 100%;
  & > .active {
    background: white;
  }
`;
const Image = styled.img`
  display: block;
  width: 50px;
  height: 50px;
  background: gray;
  background-size: cover;
  border-radius: 50%;
`;

const Avatar = styled.div`
  display: flex;
  text-align: start;
  align-items: center;
  height: 4rem;
`;
const Title = styled.div`
  margin-left: 1rem;
  font-size: 18px;
  text-transform: capitalize;
`;

export default function MenuLeft(route) {
  let { url } = route;
  const user = getUser();
  const [fullname, setFullname] = useState(user["full_name"]);

  return (
    <Menu>
      <Avatar>
        <Image src="https://img.icons8.com/plasticine/2x/user.png"></Image>
        <Title>
          <strong style={{ display: "block" }}>{fullname}</strong>
        </Title>
      </Avatar>
      <ul>
        <Item activeClassName="active">
          <IoMdPerson style={{ fontSize: 24 }}></IoMdPerson>
          <Link to={`${url}/profile`}>my information</Link>
        </Item>
        <Item activeClassName="active">
          <AiOutlineSchedule style={{ fontSize: 24 }}></AiOutlineSchedule>
          <Link to={`${url}/trips`}>My Trips</Link>
        </Item>
        <Item activeClassName="active">
          <img src={Spot}></img>
          <Link to={`${url}/spots-profile`}>My Spots</Link>
        </Item>
      </ul>
    </Menu>
  );
}
