import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import serverURL, { getUser, getToken } from '../../utils/Common';
import axios from 'axios';
const Input = styled.input`
  width: 100%;
  padding: 0.5rem 1rem;
  border-radius: 4px;
  border: 1px solid gray;
  outline: none;
`;

const Title = styled.label`
  text-align: start;
  width: 10rem;
  font-size: 1rem;
  font-weight: 100;
`;

const InputGroup = styled.div`
  display: flex;
  align-items: center;
  margin: 1.5rem 0;
`;

const Form = styled.form`
  padding: 1rem 5rem 2rem 2rem;
  background: white;
  border-radius: 4px;
`;

const Button = styled.button`
  display: flex;
  padding: 0.7rem 2.5rem;
  margin-left: auto;
  border: 1px solid gray;
  color: #000000;
  text-transform: uppercase;
  background-color: #ffffff;
  font-weight: 600;
  &:hover {
    background-color: #81ecec;
    color: #ffffff;
  }
`;

const Header = styled.h4`
  display: flex;
  align-items: center;
  text-align: start;
  text-transform: capitalize;
  height: 4rem;
`;

export default function Profile(props) {
  const user = getUser();

  const [email, setEmail] = useState(user.email);
  const [gender, setGender] = useState(user.gender);

  const [full_name, setFullname] = useState(user['full_name']);
  const [phone_num, setPhone] = useState(user['phone_num']);
  const isFill = false;
  function onClick() {
    var config = {
      headers: {
        'auth-token': getToken(),
      },
    };
    const newState = {
      full_name,
      gender,
      email,
      phone_num,
    };
    axios
      .put(`${serverURL}/auth/profile`, newState, config)
      .then((response) => {
        console.log(response.data);
        localStorage.setItem('user', JSON.stringify(response.data.data));
      })
      .catch((error) => console.log(error));
  }
  return (
    <div>
      <Header>my information</Header>
      <Form>
        <InputGroup>
          <Title>Full Name</Title>
          <Input
            name="fullname"
            value={full_name}
            onChange={(event) => setFullname(event.target.value)}
          />
        </InputGroup>
        <InputGroup>
          <Title>Email</Title>
          <Input
            type="email"
            name="email"
            value={email}
            onChange={(event) => setEmail(event.target.value)}
          />
        </InputGroup>
        <InputGroup>
          <Title>Phone Number</Title>
          <Input
            name="phonenumber"
            value={phone_num}
            onChange={(event) => setPhone(event.target.value)}
          />
        </InputGroup>
        <Button onClick={onClick()}>Save</Button>
      </Form>
    </div>
  );
}
