import React from "react";
import styled from "styled-components";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  useRouteMatch,
} from "react-router-dom";

import Profile from "./Profile";
import MenuLeft from "./MenuLeft";
import SpotsProfile from "./SpotsProfile";
import TripProfile from "./TripsProfile";
const Container = styled.div`
  display: flex;
  margin: 4rem 15%;
  padding: .5rem;
  box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.75);

`;
const Menu = styled.div`
  width: 30%;
  padding: 0 0.5rem;
  border-right: 1px solid gray;
  
`;
const Content = styled.div`
  padding: 0.5rem 2rem;
  width: 100%;
  
`;
export default function Info() {
  let { path, url } = useRouteMatch();
  return (
    <div style={{paddingBottom: ".5rem"}}>
    <Container>
      <Router>
        <Menu style={{backgroundColor:"white"}}>
          <MenuLeft url={url} />
        </Menu>
        <Content style={{backgroundColor:"white"}} >
          <Switch>
            <Route path={`${path}/profile`}>
              <Profile />
            </Route>
            <Route path={`${path}/spots-profile`}>
              <SpotsProfile />
            </Route>
            <Route path={`${path}/trips`}>
              <TripProfile />
            </Route>
          </Switch>
        </Content>
      </Router>
    </Container>
    </div>
  );
}
