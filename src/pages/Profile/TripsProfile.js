import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { BrowserRouter as Router, Link } from 'react-router-dom';
import { MdAddCircle } from 'react-icons/md';
import serverURL, { getToken } from '../../utils/Common';
import axios from 'axios';

import { FaCalendarAlt } from 'react-icons/fa';
import { IoIosArrowForward, IoIosArrowBack, IoIosPerson } from 'react-icons/io';
import { default as NumberFormat } from 'react-number-format';

const Header = styled.h4`
  display: flex;
  align-items: center;
  text-align: start;
  text-transform: capitalize;
  height: 4rem;
`;
const Content = styled.div`
  display: flex;
  flex-wrap: wrap;
`;
const Card = styled.div`
  width: 220px;
  margin: 0.5rem;
  border: 1px solid gray;
`;
const Title = styled.a`
  font-size: 17px;
  width: 100%;
  text-align: center;
  color: black;
  font-weight: 600;
  padding: 0.5rem !important;
  &:hover {
    text-decoration: none;
    color: inherit;
  }
  border: 0;
`;
const Meta = styled.div``;
const Address = styled.div`
  padding: 0.5rem;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

const Container = styled.div`
  margin: 1rem;
`;
const Button = styled.button``;
const Add = styled.a`
  text-decoration: none;
  padding: 0;
  color: black;

  &:hover {
    text-decoration: none;
    color: blue;
  }
`;
const Top = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
const Center = styled.div`
  margin-top: 3rem;
`;
const Description = styled.div`
  text-transform: capitalize;
`;
const Image = styled.img`
  display: block;
  font-size: 14px;
  height: 200px;
  line-height: 209px;
  text-size-adjust: 100%;
  width: 100%;
`;

export default function Profile() {
  const [listtrips, setTrip] = useState([]);
  useEffect(() => {
    var config = {
      headers: {
        'auth-token': getToken(),
      },
    };
    axios
      .get(`${serverURL}/trip/user`, config)
      .then((response) => {
        setTrip(response.data.data);
        console.log('Profile -> response.data.data', response.data.data);
      })
      .catch((error) => console.log(error));
  }, []);
  if (listtrips && listtrips.length > 0) {
    console.log('listTrip: ', listtrips);
    return (
      <Container>
        <Top>
          <Header>my trips</Header>
          <Add href="/trip/add">Add Trip</Add>
        </Top>
        <Content>
          {listtrips.map((trip) => (
            <Card>
              <Image src={trip['cover_image']} />
              <Meta>
                <Title href={'/trip/' + trip._id + '/detail'}>
                  {trip.name}
                </Title>{' '}
                <div style={{marginLeft:"3%"}}>
                  <div className="detail-budget">
                    <strong>VND</strong>
                    <span>
                      <NumberFormat
                        style={{ fontWeight: '500' }}
                        value={trip.budget}
                        displayType={'text'}
                        thousandSeparator={true}
                      />
                    </span>
                  </div>
                  <div style={{ display: 'flex'}}>
                    <div className="detail-time">
                      <FaCalendarAlt />
                      <span>{trip.number_of_day}</span>
                    </div>
                    <div className="detail-time">
                      <IoIosPerson />
                      <span>{trip.adult + trip.kid}</span>
                    </div>
                  </div>
                </div>
              </Meta>
            </Card>
          ))}
        </Content>
      </Container>
    );
  } else {
    return (
      <Center>
        <Title href="/trip/add">
          <MdAddCircle size={64} color="#0000000d" />
          <Description>add your trip</Description>
        </Title>
      </Center>
    );
  }
}
