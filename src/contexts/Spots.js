import React, { useState, Children, Component } from 'react';

import SpotContext from "./SpotContext";

export default class SpotProvider extends Component{
  constructor() {
		super();
		this.state = {
			spotItem: []
		};
		this.addSpotItem = this.addSpotItem.bind(this);
	}

	addSpotItem(spot) {
		console.log(spot);
		this.setState({
			spotItem: this.state.spotItem.concat(spot)
		});
	}

	render() {
		return (
			<SpotContext.Provider
				value={{
					spotItem: this.state.spotItem,
					addSpotItem: this.addSpotItem
				}}
			>
				{this.props.children}
			</SpotContext.Provider>
		);
	}
}