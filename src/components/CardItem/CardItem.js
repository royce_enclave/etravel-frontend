import React, { useState } from "react";

import { BrowserRouter as Router, Link } from "react-router-dom";

import { MdPersonOutline, MdAttachMoney } from "react-icons/md";
import { FaCalendarAlt } from "react-icons/fa";

import "./CardItem.css";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

export default function Carditem(props) {
  console.log(props);

  // const [location]= useState("Location")
  const [people] = useState("2");
  const [budget] = useState("1550");
  const [time] = useState("2 days");

  return (
    <div className="slide-card" key={props.item._id}>
      <li>
        <Link to="/info/profile">
          <div
            className="card-item"
            style={{ backgroundImage: `url(${props.item["cover_image"]})` }}
          >
            <div className="card-content">
              <h3 className="card-content__header">{props.item.name}</h3>
              <div className="card-content__details">
                <div className="detail-people">
                  <MdPersonOutline />
                  <span>{people}</span>
                </div>
                <div className="detail-budget">
                  <MdAttachMoney />
                  <span>{budget}</span>
                </div>
                <div className="detail-time">
                  <FaCalendarAlt />
                  <span>{time}</span>
                </div>
              </div>
            </div>
          </div>
        </Link>
      </li>
    </div>
  );
}
