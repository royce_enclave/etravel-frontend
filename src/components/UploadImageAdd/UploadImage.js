import React, { Component } from 'react';
import axios from 'axios';
import './UploadImage.css';
import serverURL from '../../utils/Common';
class App extends Component {
  fileObj = [];
  fileArray = [];
  constructor(props) {
    super(props);
    this.state = {
      image: null,
    };
  }

  handleImageChange = (e) => {
    this.setState({
      image: e.target.files,
    });
    this.fileObj.push(e.target.files);
    let index = this.fileObj.length - 1;
    let form_data = new FormData();
    for (let i = 0; i < this.fileObj[index].length; i++) {
      let images = this.fileObj[index][i];
      form_data.append('images', images);
    }
    axios
      .post(`${serverURL}/upload/images`, form_data)
      .then((res) => {
        for (let i = 0; i < this.fileObj[index].length; i++)
          this.fileArray.push(URL.createObjectURL(this.fileObj[index][i]));
        this.setState({
          file: this.fileArray,
          images: res.data.data,
        });
        console.log(res.data.data);
        this.props.data(this.state.images);
      })
      .catch((err) => {
        alert('Something wrong. Try again');
        window.location.href = '/spots/add';
        console.log('will test upload error: ', err);
      });
  };

  render() {
    console.log('dataImage:', this.state);
    return (
      <form onSubmit={this.handleSubmit}>
        <div
          style={{
            display: 'flex',
            overflowX: 'scroll',
          }}
        >
          {' '}
       
            <label className="custom-input">
              <input
                type="file"
                id="image"
                multiple
                accept="image/png, image/jpeg"
                onChange={this.handleImageChange}
                required
              />
            </label>
         
          <div className="imgPreview">
            {(this.fileArray || []).map((url) => (
              <img src={url} alt="..." />
            ))}
          </div>
        </div>
      </form>
    );
  }
}

export default App;
