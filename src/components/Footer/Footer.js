import React from "react";
import styled from "styled-components";
import Button from "./Button";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Team from "./Team";

const Footer = styled.div`
  display: block;
`;
function Copyright() {
  return (
    <Typography  align="center">
      {"Copyright © "}
      {"eTravel "}
      {new Date().getFullYear()}
    </Typography>
  );
}
const Top = styled.div`
  display: flex;
  justify-content: space-between;
  background: sliver;
  padding: 3rem 17.5%;
`;
const Bottom = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 3rem;
  color: #ffffff;
  background: #000000;
`;
const TopRight = styled.div`
  display: flex;
  > div {
    display: inherit;
  }
`;

export default function FooterPage() {
  return (
    <Footer>
      <Top>
        <Team></Team>

        <TopRight>
          <Button />
        </TopRight>
      </Top>
      <Bottom>
        <Box>
          <Copyright />
        </Box>
      </Bottom>
    </Footer>
  );
}
