import React from "react";
import styled from "styled-components";

const Title = styled.h4`
  text-transform: uppercase;
`;
const Item = styled.li`
  font-style: italic;
`;
export default function Team() {
  return (
    <div>
      <Title>Team Members</Title>
      <ul style={{textAlign:"start"}}>
        <Item>Hoang (Hamish) L. DANG </Item>
        <Item>Song Toan (Royce) N. LE </Item>
        <Item>Quang (Basil) N. DANG</Item>
        <Item>Qui (Will) T. NGUYEN</Item>
        <Item>Minh (Miller) Q. LUU </Item>
      </ul>
    </div>
  );
}
