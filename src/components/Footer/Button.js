import React from "react";
import styled from "styled-components";
import QR from "./QR.png";
import googleplay from "./android.svg";
import appstrore from "./appstore.svg";

const ChPlay = styled.div`
  position: relative;
  padding-bottom: 5%;
`;

const AppStore = styled.div``;
const AppIcon = styled.img`
  width: 100%;
  height: 56px;
`;
const Qr = styled.img`
  width: 128px;
  height: 128px;
`;

const App = styled.div`
  display: block;
`;

export default function ButtonStore() {
  return (
    <div>
      <Qr src={QR} alt=""></Qr>
      <App>
        <ChPlay>
          <AppIcon src={googleplay} alt="" />
        </ChPlay>
        <AppStore>
          <AppIcon src={appstrore} alt="" />
        </AppStore>
      </App>
    </div>
  );
}
