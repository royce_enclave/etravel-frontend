import React from 'react';
import Axios from 'axios';
import Slider from 'react-slick';
import { MdAttachMoney } from 'react-icons/md';
import { FaRegClock } from 'react-icons/fa';
import { BrowserRouter as Router, Link } from 'react-router-dom';
import { IoIosArrowForward, IoIosArrowBack } from 'react-icons/io';
import { default as NumberFormat } from 'react-number-format';
import '../CardItem/CardItem.css';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import serverURL from '../../utils/Common';
export default class App extends React.Component {
  state = {
    spots: [],
  };

  componentDidMount() {
    Axios.get(`${serverURL}/spot`)
      .then((res) => {
        this.setState({ spots: res.data.data });
      })
      .catch((err) => {
        console.log('weeeee', err.response.data);
      });
  }
  render() {
    const settings = {
      className: 'center',
      centerPadding: '60px',
      infinite: true,
      slidesToShow: 4,
      slidesToScroll: 1,
      nextArrow: <IoIosArrowForward size={64} color="black" />,
      prevArrow: <IoIosArrowBack size={64} color="black" />,
    };
    return (
      <div
        className="body-picture-setup"
        style={{ width: '80%', height: '90%', marginLeft: '10%' }}
      >
        <Slider {...settings}>
          {this.state.spots.slice(0, 12).map((item) => (
            <div className="slide-card" key={item._id}>
              <li>
                <Link
                  to={'/spot/' + item._id + '/detail'}
                  style={{ marginTop: '10px' }}
                >
                  <div
                    className="card-items"
                    style={{ backgroundImage: `url(${item['cover_image']})` }}
                  >
                    <div className="card-content">
                      <h3
                        className="card-content__header"
                        style={{
                          color: 'white',
                          whiteSpace: 'nowrap',
                          overflow: 'hidden',
                          textOverflow: 'ellipsis',
                        }}
                      >
                        {item.name}
                      </h3>
                      <div className="card-content__details">
                        <div className="detail-budget">
                          VND{' '}
                          <span>
                            <NumberFormat
                              value={item.price}
                              displayType={'text'}
                              thousandSeparator={true}
                            />
                          </span>
                        </div>
                        <div className="detail-time">
                          <FaRegClock />
                          <span>{item.time_duration}</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </Link>
              </li>
            </div>
          ))}
        </Slider>
      </div>
    );
  }
}
