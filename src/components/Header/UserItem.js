import React from "react";
import styled from "styled-components";
import { BrowserRouter as Router, Link } from "react-router-dom";
import { removeUserSession, getUser, getToken } from "../../utils/Common";

import Info from "./info.svg";
import Logout from "./logout.svg"
import "./Header.css";

export default function Userdrop() {
  const user = getUser();
  const token = getToken();

  const Avatar = styled.img`
    width: 3rem;
    height: 3rem;
    border-radius: 50%;
    margin-right: 0.5rem;
    background: gray;
  `;
  const Button = styled.button`
    background: inherit;
    padding: 1rem;
    outline: none;
    border: 0;
    color: red;
    text-align: start;
    width: 100%;
    &:hover {
      text-decoration: none;
    }
    
  `;

  const Content = styled.div`
    display: none;
    position: absolute;
    top: 4rem;
    right: 0;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
    z-index: 1;
  `;
  const Dropdown = styled.div`
    position: relative;
    display: ${token ? "flex" : "hidden"};
    justify-content: center;
    align-items: center;
    margin-right: 1rem;
    &:hover ${Content} {
      display: block;
    }
  `;
  const Item = styled(Link)`
    text-align: start;
    padding: 1rem !important;
    color: black;
    width: 100%;
    &:hover{
      text-decoration: none;
      color: black;
    }
  `
    const Icon= styled.img`
    width: 24px;
    `
    const Li = styled.div`
      display: flex;
      justify-content: center;
      margin: 0 1rem;
    `
  
  const handleLogout = () => {
    removeUserSession();
    window.location.href = "/";
  };


  if (token) {
    return (
      <Dropdown>
        <Avatar src="https://img.icons8.com/plasticine/2x/user.png"></Avatar>
        <div className="dropdown">
          <span className="">{user["full_name"]}</span>
          <Content>
            <ul>
              <Li>
                <Icon src={Info}></Icon>
                <Item to="/info/profile">Infomation</Item>
              </Li>
              <Li>
                <Icon src={Logout}></Icon>
                <Button onClick={handleLogout}>Logout</Button>
              </Li>
            </ul>
          </Content>
        </div>
      </Dropdown>
    );
  } else {
    return <div></div>;
  }
}
