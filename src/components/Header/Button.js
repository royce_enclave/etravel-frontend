import React from "react";
import classnames from "classnames";
import styled from "styled-components";
import { BrowserRouter as Router, Link, Switch, Route } from "react-router-dom";
import { getUser, getToken } from "../../utils/Common";

const Button = styled.button`
  margin-left: 0.5rem;
  background: #ffffff;
  outline: none;
  width:90px;
  border: 1px solid gray;
  color: black;
  &:last-chill {
    background: #000000;
    color: #ffffff;
  }
  &:hover {
    text-decoration: none;
    background: #81ecec;
    color: white;
    transition: 0.4s;
  }
 
`;
const Item = styled(Link)`
color: black;
  &:hover{
    text-decoration: none;
    color: white;
  }
`
const Right = styled.ul`
  display: flex;
  margin-bottom: 0;
`;

export default function HeaderButton() {
  const user = getUser();
  const token = getToken();

  return (
    <div className={classnames("group-btn", { hidden: token })}>
      <Right>
        <Button>
          <Item to={"/login"}>Login</Item>
        </Button>
        <Button>
          <Item to={"/register"}>Register</Item>
        </Button>
      </Right>
    </div>
  );
}
