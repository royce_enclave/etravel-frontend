import React from "react";
import styled from "styled-components";
import Logo from "./logo.png";

import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import User from "./UserItem";
import Button from "./Button";
import {clearStorage} from "../../utils/Common";

const Header = styled.div`
  background: white;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0 11%;
  border-bottom: 1px solid gray;
  position: fixed;
  width: 100%;
  z-index: 9990;
`;
const Right = styled.div`
  display: flex;
`;

export default function Headers() {
  return (
    <Header>
      <div className="header-left">
        <Link className="navbar-brand"  to={"/"}>
          <img src={Logo} alt="Logo" />
        </Link>
      </div>
      <Right>
        <User />
        <Button />
      </Right>
    </Header>
  );
}
