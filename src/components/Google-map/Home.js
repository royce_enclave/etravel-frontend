import React, { Component } from "react";
import Map from "./Map";

class Home extends Component {
  dataa = (dt) =>{
    this.props.data(dt)
  }
  render() {
    return (
      <div>
        <Map
          google={this.props.google}
          center={{ lat: 16.062737434006507, lng: 108.2056857201294 }}
          height="500px"
          zoom={15}
          data={this.dataa}
        />
      </div>
    );
  }
}

export default Home;
