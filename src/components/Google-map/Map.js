import React, { Component } from "react";
import slugify from "slugify";
import {
  withGoogleMap,
  GoogleMap,
  withScriptjs,
  InfoWindow,
  Marker,
} from "react-google-maps";
import Geocode from "react-geocode";
// import Autocomplete from "react-google-autocomplete";
Geocode.setApiKey("AIzaSyCXvbLCXHsiGHMRxbC_JLn-2wnKOPdrnsE");
Geocode.enableDebug();
slugify.extend({ Đ: "D" });
slugify.extend({ đ: "d" });

class Map extends Component {
  constructor(props) {
    super(props);
    this.state = {
      address: "",
      city: "",
      district: "",
      street: "",
      street_number: "",
      country: "Việt Nam",
      mapPosition: {
        lat: this.props.center.lat,
        lng: this.props.center.lng,
      },
      markerPosition: {
        lat: this.props.center.lat,
        lng: this.props.center.lng,
      },
    };
  }
  /**
   * Get the current address from the default map position and set those values in the state
   */
  componentDidMount() {
    Geocode.fromLatLng(
      this.state.mapPosition.lat,
      this.state.mapPosition.lng
    ).then(
      (response) => {
        console.log('will test', response);
        const address = response.results[0].formatted_address,
        addressArray = response.results[0].address_components,
        city = this.getCity(addressArray),
        district = this.getDistrict(addressArray),
        street = this.getStreet(addressArray);
        let street_number = this.getNumber(addressArray);

        this.setState({
          address: address ? address : "",
          district: district ? district : "",
          city: city ? city : "",
          street: street ? street : "",
          street_number: street_number ? street_number : "",
        });
      },
      (error) => {
        console.error(error);
      }
    );
  }
  /**
   * Component should only update ( meaning re-render ), when the user selects the address, or drags the pin
   *
   * @param nextProps
   * @param nextState
   * @return {boolean}
   */
  shouldComponentUpdate(nextProps, nextState) {
    return !(JSON.stringify(nextState) == JSON.stringify(this.state));
  }
  /**
   * Get the city and set the city input value to the one selected
   *
   * @param addressArray
   * @return {string}
   */
  getCity = (addressArray) => {
    let city = "";
    for (let i = 0; i < addressArray.length; i++) {
      if (
        addressArray[i].types[0] &&
        "administrative_area_level_1" === addressArray[i].types[0]
      ) {
        city = addressArray[i].long_name;
        return city;
      }
    }
  };
  /**
   * Get the area and set the area input value to the one selected
   *
   * @param addressArray
   * @return {string}
   */
  getDistrict = (addressArray) => {
    let district = "";
    for (let i = 0; i < addressArray.length; i++) {
      for (let i = 0; i < addressArray.length; i++) {
        if (
          addressArray[i].types[0] &&
          "administrative_area_level_2" === addressArray[i].types[0]
        ) {
          district = addressArray[i].long_name;
          return district;
        }
      }
    }
  };
  /**
   * Get the address and set the address input value to the one selected
   *
   * @param addressArray
   * @return {string}
   */
  getStreet = (addressArray) => {
    let street = "";
    for (let i = 0; i < addressArray.length; i++) {
      for (let i = 0; i < addressArray.length; i++) {
        if (addressArray[i].types[0] && "route" === addressArray[i].types[0]) {
          street = addressArray[i].long_name;
          return street;
        }
      }
    }
  };
 
  getNumber = (addressArray) => {
    let street_number = "";
    for (let i = 0; i < addressArray.length; i++) {
      for (let i = 0; i < addressArray.length; i++) {
        if (addressArray[i].types[0] && "street_number" === addressArray[i].types[0]) {
          street_number = addressArray[i].long_name;
          return street_number;
        }
      }
    }
  };
  /**
   * And function for city,state and address input
   * @param event
   */
  onChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };
  /**
   * This Event triggers when the marker window is closed
   *
   * @param event
   */
  onInfoWindowClose = (event) => {};

  /**
   * When the marker is dragged you get the lat and long using the functions available from event object.
   * Use geocode to get the address, city, area and state from the lat and lng positions.
   * And then set those values in the state.
   *
   * @param event
   */
  onMarkerDragEnd = (event) => {
    let newLat = event.latLng.lat(),
      newLng = event.latLng.lng();

    Geocode.fromLatLng(newLat, newLng).then(
      (response) => {
        console.log("map test: ", response.results[0].address_components);
        const addressArray = response.results[0].address_components,
          city = slugify((this.getCity(addressArray)?this.getCity(addressArray): ""), ' '),
          district = slugify((this.getDistrict(addressArray))?this.getDistrict(addressArray): "",' '),
          street = slugify((this.getStreet(addressArray)? this.getStreet(addressArray): "Unknow road"),' '),
          country = slugify((this.state.country?this.state.country: ""),' '),
          street_number = slugify(this.getNumber(addressArray)? this.getNumber(addressArray) : "",) 
        let fullAddress = street_number+" "+street+", "+district+", "+city+", "+ country;
        this.props.data({ newLat, newLng, city, district, street, fullAddress, street_number });

        this.setState({
          address: fullAddress,
          district: district ? district : "",
          city: city ? city : "",
          street: street ? street : "",
          street_number: street_number ? street_number : "",
          markerPosition: {
            lat: newLat,
            lng: newLng,
          },
          mapPosition: {
            lat: newLat,
            lng: newLng,
          },
        });
      },
      (error) => {
        console.error(error);
      }
    );
  };

  /**
   * When the user types an address in the search box
   * @param place
   */
  onPlaceSelected = (place) => {
    console.log("plc", place);
    const address = place.formatted_address,
      addressArray = place.address_components,
      city = this.getCity(addressArray),
      district = this.getDistrict(addressArray),
      street = this.getStreet(addressArray),
      latValue = place.geometry.location.lat(),
      lngValue = place.geometry.location.lng();
    // Set these values in the state.
    this.setState({
      address: address ? address : "",
      district: district ? district : "",
      city: city ? city : "",
      street: street ? street : "",
      markerPosition: {
        lat: latValue,
        lng: lngValue,
      },
      mapPosition: {
        lat: latValue,
        lng: lngValue,
      },
    });
  };

  render() {
    const AsyncMap = withScriptjs(
      withGoogleMap((props) => (
        <GoogleMap
          google={this.props.google}
          defaultZoom={this.props.zoom}
          defaultCenter={{
            lat: this.state.mapPosition.lat,
            lng: this.state.mapPosition.lng,
          }}
        >
          {/* InfoWindow on top of marker */}
          <InfoWindow
            onClose={this.onInfoWindowClose}
            position={{
              lat: this.state.markerPosition.lat + 0.0018,
              lng: this.state.markerPosition.lng,
            }}
          >
            <div>
              <span style={{ padding: 0, margin: 0 }}>
                {this.state.address}
              </span>
            </div>
          </InfoWindow>
          {/*Marker*/}
          <Marker
            google={this.props.google}
            draggable={true}
            onDragEnd={this.onMarkerDragEnd}
            position={{
              lat: this.state.markerPosition.lat,
              lng: this.state.markerPosition.lng,
            }}
          />
          <Marker />
          {/* For Auto complete Search Box 
          <Autocomplete
            style={{
              width: "100%",
              height: "40px",
              paddingLeft: "16px",
              marginTop: "2px",
            }}
            onPlaceSelected={this.onPlaceSelected}
            types={["(cities)"]}
            componentRestrictions={{country: "vn"}}

          
          />*/}
        </GoogleMap>
      ))
    );
    let map;
    if (this.props.center.lat !== undefined) {
      map = (
        <div>
          Drag marker to select the place<strong  style={{color:"red"}}>*</strong>
          <AsyncMap
            googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXvbLCXHsiGHMRxbC_JLn-2wnKOPdrnsE&libraries=places"
            loadingElement={<div style={{ height: `100%` }} />}
            containerElement={<div style={{ height: this.props.height }} />}
            mapElement={<div style={{ height: `100%` }} />}
          />
          <div>
            <div className="form-group">
              <label htmlFor="">Address</label>
              <input
                type="text"
                name="address"
                className="form-controls"
                onChange={this.onChange}
                readOnly={true}
                value={this.state.address}
              />
            </div>
          </div>
        </div>
      );
    } else {
      map = <div style={{ height: this.props.height }} />;
    }
    return map;
  }
}
export default Map;
