import React, { Component } from 'react';
import '../Banner/Banner.css';
import { getToken, clearStorage } from '../../utils/Common';
export default class Banner extends Component {
  onClick() {
    if(getToken())
    clearStorage()
    if (getToken()) {
      window.location.href = '/trip/add';
    } else {
      alert('You must login first');
      window.location.href = '/login';
    }
  }
  render() {
    return (
      <div className="banner">
        <div className="page-content">
          <h1>eTravel</h1>
          <br />
          <p>Enjoy your vacation with eTravel!</p>
          <br />
          <a onClick={this.onClick} >Create Your Trip </a>
        </div>
      </div>
    );
  }
}
